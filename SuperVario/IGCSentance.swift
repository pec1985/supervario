//
//  IGCSentance.swift
//  SuperVario
//
//  Created by Pedro Enrique on 3/17/17.
//  Copyright © 2017 Pedro Enrique. All rights reserved.
//

import Foundation
import CoreLocation

struct IGCSentance
{
    var line:String!
    var location: CLLocationCoordinate2D!
    var gpsAltitude: Measurement<UnitLength>!
    var baroAltitude: Measurement<UnitLength>!
    var speed: Measurement<UnitSpeed>!
    var timeStamp:String!
	var climbRate: Measurement<UnitSpeed>!

	
	func isEqual(to other:IGCSentance?) -> Bool {
		if other == nil { return false }
		return self.line == other!.line
	}

    /**
     B1101355206343N00006198WA0058700558
     
     B         record type is a basic tracklog record
     110135    <time> tracklog entry was recorded at 11:01:35 i.e. just after 11am
     5206343N  <lat> i.e. 52 degrees 06.343 minutes North
     00006198W <long> i.e. 000 degrees 06.198 minutes West
     A         <alt valid flag> confirming this record has a valid altitude value
     00587     <altitude from pressure sensor>
     00558     <altitude from GPS>
     */
    init(varioData:VariometerData, timeStamp:String)
    {
        let stirngLat = varioData.igcLatitude
        let stringLon = varioData.igcLongitude
        
        let gpsAlt = self.igcAltitude(meters: Int(varioData.gpsAltitude.value))
        let baroAlt = self.igcAltitude(meters: Int(varioData.baroAltitude.value))
        let result = "B\(timeStamp)\(stirngLat)\(stringLon)A\(baroAlt)\(gpsAlt)\r\n"
        
        self.line = result
        self.location = CLLocationCoordinate2D()
        self.location.latitude = self.convertDegreeAngle(point: stirngLat, isLat:  true)
        self.location.longitude = self.convertDegreeAngle(point: stringLon, isLat:  false)
        self.gpsAltitude = varioData.gpsAltitude
        self.baroAltitude = varioData.baroAltitude
        self.speed = varioData.speed
        self.timeStamp = timeStamp
		self.climbRate = varioData.climbRate
    }
    
    init(line:String) {

        let lat = line.substring(with: line.range(7,8))
        let lon = line.substring(with: line.range(15,9))
        let baroAlt = line.substring(with: line.range(25,5))
        let gpsAlt = line.substring(with: line.range(30,5))
        
        self.line = line
        self.location = CLLocationCoordinate2D()
        self.location.latitude = self.convertDegreeAngle(point:lat, isLat: true)
        self.location.longitude = self.convertDegreeAngle(point:lon, isLat:false)
        self.gpsAltitude = Measurement(value: gpsAlt.asDouble(), unit: .meters)
        self.baroAltitude = Measurement(value: baroAlt.asDouble(), unit: .meters)
        self.speed = Measurement(value: 0, unit: .metersPerSecond)
        self.timeStamp = line.substring(with: line.range(1,6))
		self.climbRate = Measurement(value: 0, unit: .metersPerSecond)
    }
    
    private func convertDegreeAngle(point: String, isLat:Bool) -> CLLocationDegrees
    {
        let multiplier = (point.contains("S") || point.contains("W")) ? -1.0 : 1.0
        var degrees:Double
        var minutes:Double
        if (isLat) {
            degrees = point.substring(with: point.range(0,2)).asDouble()
            minutes = point.substring(with: point.range(2,2)).asDouble() + (point.substring(with: point.range(4,3)).asDouble() / 1000)
        } else {
            degrees = point.substring(with: point.range(0,3)).asDouble()
            minutes = point.substring(with: point.range(3,2)).asDouble() + (point.substring(with: point.range(5,3)).asDouble() / 1000)
        }
        return (degrees + (minutes / 60)) * multiplier;
    }

    
    private func igcAltitude(meters num: Int) -> String {
        let meters = "\(num)"
        if meters.startsWith("-") { return "00000" }
        let diff = 5 - meters.characters.count
        var result = ""
        for _ in 0..<diff {
            result += "0"
        }
        result.append(meters)
        return result
    }
    
    // Not used, but kept for reference
    //
    //    private func converCords(_ val: Double, isLat:Bool) -> String
    //    {
    //        let absValue = abs(val)
    //        let d = floor(absValue)
    //        let m = round((absValue - d) * 60 * 1000)
    //
    //        var letter = ""
    //        var degrees = ""
    //        var minutes = ""
    //        if isLat {
    //            if val > 0 {
    //                letter = "N"
    //            } else {
    //                letter = "S"
    //            }
    //            if d < 10 {
    //                degrees = "0\(Int(d))"
    //            } else {
    //                degrees = "\(Int(d))"
    //            }
    //        } else {
    //            if val > 0 {
    //                letter = "E"
    //            } else {
    //                letter = "W"
    //            }
    //            if d < 100 {
    //                degrees = "00\(Int(d))"
    //            } else if d < 10 {
    //                degrees = "0\(Int(d))"
    //            } else {
    //                degrees = "\(Int(d))"
    //            }
    //        }
    //
    //        if m < 10000 {
    //            minutes = "0\(Int(m))"
    //        } else {
    //            minutes = "\(Int(m))"
    //        }
    //        let result = "\(degrees)\(minutes)\(letter)"
    //        
    //        return result
    //    }

}
