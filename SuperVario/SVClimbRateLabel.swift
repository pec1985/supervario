//
//  SVClimbRateLabel.swift
//  SuperVario
//
//  Created by Pedro Enrique on 2/27/17.
//  Copyright © 2017 Pedro Enrique. All rights reserved.
//

import UIKit

@IBDesignable class SVClimbRateLabel: SVLabel
{
    private var climbRate:Double = 0
    override func didLoad() {
        super.didLoad()
        self.text = "0 fpm"
    }

    override func updateData(data: VariometerData)
    {
        let climbRate = data.climbRate.value
        if climbRate == self.climbRate { return }
        self.climbRate = climbRate

        let feetPerMinute = data.climbRate.converted(to: .feetPerMinute).value
        let result = Int(feetPerMinute / 10) * 10
        self.text = "\(result.commaSeparated()) fpm"
    }

}
