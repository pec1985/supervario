//
//  DFBlunoManager.m
//
//  Created by Seifer on 13-12-1.
//  Copyright (c) 2013年 DFRobot. All rights reserved.
//

#import "DFBlunoManager.h"

#define kBlunoService @"dfb0"
#define kBlunoDataCharacteristic @"dfb1"

@interface DFBlunoManager ()
{
    BOOL _bSupported;
    dispatch_queue_t _backgroundQueue;
}

@property (strong,nonatomic) CBCentralManager* centralManager;
@property (strong,nonatomic) NSMutableDictionary* dicBleDevices;
@property (strong,nonatomic) NSMutableDictionary* dicBlunoDevices;
@property (strong,nonatomic) NSMutableData* receivedData;

@end

@implementation DFBlunoManager

#pragma mark- Functions

+ (id)sharedInstance
{
	static DFBlunoManager* this	= nil;
    
	if (!this)
    {
		this = [[DFBlunoManager alloc] init];
        this.dicBleDevices = [[NSMutableDictionary alloc] init];
        this.dicBlunoDevices = [[NSMutableDictionary alloc] init];
        this->_bSupported = NO;
        this->_receivedData = [NSMutableData data];
        this.runOnMainThread = YES;
    }
    
    return this;
}

-(void)setDelegate:(id<DFBlunoDelegate>)aDelegate
{
    _delegate = aDelegate;
    _backgroundQueue = dispatch_queue_create("dfrobot.bluetooth", DISPATCH_QUEUE_SERIAL);
    self.centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:_backgroundQueue];

}

- (void)configureSensorTag:(CBPeripheral*)peripheral
{
    
    CBUUID *sUUID = [CBUUID UUIDWithString:kBlunoService];
    CBUUID *cUUID = [CBUUID UUIDWithString:kBlunoDataCharacteristic];
    
    [BLEUtility setNotificationForCharacteristic:peripheral sCBUUID:sUUID cCBUUID:cUUID enable:YES];
    NSString* key = [peripheral.identifier UUIDString];
    DFBlunoDevice* blunoDev = [self.dicBlunoDevices objectForKey:key];
    blunoDev->_bReadyToWrite = YES;
    if ([((NSObject*)_delegate) respondsToSelector:@selector(readyToCommunicate:)])
    {
        if (self.runOnMainThread) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [_delegate readyToCommunicate:blunoDev];
            });
        } else {
            [_delegate readyToCommunicate:blunoDev];
        }
    }
    
}

- (void)deConfigureSensorTag:(CBPeripheral*)peripheral
{
    
    CBUUID *sUUID = [CBUUID UUIDWithString:kBlunoService];
    CBUUID *cUUID = [CBUUID UUIDWithString:kBlunoDataCharacteristic];
    
    [BLEUtility setNotificationForCharacteristic:peripheral sCBUUID:sUUID cCBUUID:cUUID enable:NO];
    
}

- (void)scan
{
    [self.centralManager stopScan];
    //[self.dicBleDevices removeAllObjects];
    //[self.dicBlunoDevices removeAllObjects];
    if (_bSupported)
    {
        [self.centralManager scanForPeripheralsWithServices:@[[CBUUID UUIDWithString:kBlunoService]] options:nil];
    }

}

- (void)stop
{
    [self.centralManager stopScan];
}

- (void)clear
{
    [self.dicBleDevices removeAllObjects];
    [self.dicBlunoDevices removeAllObjects];
}

- (void)connectToDevice:(DFBlunoDevice*)dev
{
    BLEDevice* bleDev = [self.dicBleDevices objectForKey:dev.identifier];
    [bleDev.centralManager connectPeripheral:bleDev.peripheral options:nil];
}

- (void)disconnectToDevice:(DFBlunoDevice*)dev
{
    BLEDevice* bleDev = [self.dicBleDevices objectForKey:dev.identifier];
    [self deConfigureSensorTag:bleDev.peripheral];
    [bleDev.centralManager cancelPeripheralConnection:bleDev.peripheral];
}

- (void)writeDataToDevice:(NSData*)data Device:(DFBlunoDevice*)dev
{
    dispatch_block_t block = ^{
        if (!_bSupported || data == nil)
        {
            return;
        }
        else if(!dev.bReadyToWrite)
        {
            return;
        }
        BLEDevice* bleDev = [self.dicBleDevices objectForKey:dev.identifier];
        [BLEUtility writeCharacteristic:bleDev.peripheral sUUID:kBlunoService cUUID:kBlunoDataCharacteristic data:data];
    };
    if (_runOnMainThread) {
        dispatch_async(dispatch_get_main_queue(), block);
    } else {
        dispatch_async(_backgroundQueue, block);
    }
}

#pragma mark - CBCentralManager delegate

-(void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    if (central.state != CBManagerStatePoweredOn)
    {
        _bSupported = NO;
        NSArray* aryDeviceKeys = [self.dicBlunoDevices allKeys];
        for (NSString* strKey in aryDeviceKeys)
        {
            DFBlunoDevice* blunoDev = [self.dicBlunoDevices objectForKey:strKey];
            blunoDev->_bReadyToWrite = NO;
        }
        
    }
    else
    {
        _bSupported = YES;
        
    }
    
    if ([((NSObject*)_delegate) respondsToSelector:@selector(bleDidUpdateState:)])
    {
        if (self.runOnMainThread) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [_delegate bleDidUpdateState:_bSupported];
            });
        } else {
            [_delegate bleDidUpdateState:_bSupported];
        }
    }
    
}

-(void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    NSString* key = [peripheral.identifier UUIDString];
    BLEDevice* dev = [self.dicBleDevices objectForKey:key];
    if (dev !=nil )
    {
        //if ([dev.peripheral isEqual:peripheral])
        {
            dev.peripheral = peripheral;
            if ([((NSObject*)_delegate) respondsToSelector:@selector(didDiscoverDevice:)])
            {
                DFBlunoDevice* blunoDev = [self.dicBlunoDevices objectForKey:key];
                if (self.runOnMainThread) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_delegate didDiscoverDevice:blunoDev];
                    });
                } else {
                    [_delegate didDiscoverDevice:blunoDev];
                }
            }
        }
    }
    else
    {
        BLEDevice* bleDev = [[BLEDevice alloc] init];
        bleDev.peripheral = peripheral;
        bleDev.centralManager = self.centralManager;
        [self.dicBleDevices setObject:bleDev forKey:key];
        DFBlunoDevice* blunoDev = [[DFBlunoDevice alloc] init];
        blunoDev.identifier = key;
        blunoDev.name = peripheral.name;
        [self.dicBlunoDevices setObject:blunoDev forKey:key];

        if ([((NSObject*)_delegate) respondsToSelector:@selector(didDiscoverDevice:)])
        {
            if (self.runOnMainThread) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_delegate didDiscoverDevice:blunoDev];
                });
            } else {
                [_delegate didDiscoverDevice:blunoDev];
            }
        }
    }
}


-(void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    peripheral.delegate = self;
    [peripheral discoverServices:nil];
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    NSString* key = [peripheral.identifier UUIDString];
    DFBlunoDevice* blunoDev = [self.dicBlunoDevices objectForKey:key];
    blunoDev->_bReadyToWrite = NO;
    if ([((NSObject*)_delegate) respondsToSelector:@selector(didDisconnectDevice:)])
    {
        if (self.runOnMainThread) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [_delegate didDisconnectDevice:blunoDev];
            });
        } else {
            [_delegate didDisconnectDevice:blunoDev];
        }
    }
}

#pragma  mark - CBPeripheral delegate
-(void) peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    for (CBService *s in peripheral.services) [peripheral discoverCharacteristics:nil forService:s];
}

-(void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    if ([service.UUID isEqual:[CBUUID UUIDWithString:kBlunoService]])
    {
        [self configureSensorTag:peripheral];
    }

}


-(void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{

}

-(void)sendData:(NSData*)data withId:(NSString*)identifier
{
    if ([((NSObject*)_delegate) respondsToSelector:@selector(didReceiveData:device:)])
    {
        DFBlunoDevice* blunoDev = [self.dicBlunoDevices objectForKey:identifier];
        if (self.runOnMainThread) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [_delegate didReceiveData:data device:blunoDev];
            });
        } else {
            [_delegate didReceiveData:data device:blunoDev];
        }
    }
}

-(void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if ([self terminator] != nil) {
        char terminator = [[self terminator] characterAtIndex:0];
        const char* data = [characteristic.value bytes];
        NSUInteger length = [[characteristic value] length];
        for (NSUInteger i = 0; i < length; i++) {
            char c = data[i];
            if (terminator == c) {
                [self sendData:[[self receivedData] copy] withId:[peripheral.identifier UUIDString]];
                [self setReceivedData:[NSMutableData data]];
            } else {
                [[self receivedData] appendBytes:&c length:1];
            }
        }
    } else {
        [self setReceivedData:[characteristic.value copy]];
        [self sendData:[[self receivedData] copy] withId:[peripheral.identifier UUIDString]];
    }
}

-(void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if ([((NSObject*)_delegate) respondsToSelector:@selector(didWriteData:)])
    {
        NSString* key = [peripheral.identifier UUIDString];
        DFBlunoDevice* blunoDev = [self.dicBlunoDevices objectForKey:key];
        if (self.runOnMainThread) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [_delegate didWriteData:blunoDev];
            });
        } else {
            [_delegate didWriteData:blunoDev];
        }
    }
    
}

@end
