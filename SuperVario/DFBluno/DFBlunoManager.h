//
//  DFBlunoManager.h
//
//  Created by Seifer on 13-12-1.
//  Copyright (c) 2013年 DFRobot. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BLEUtility.h"
#import "BLEDevice.h"
#import "DFBlunoDevice.h"

@protocol DFBlunoDelegate <NSObject>
@required

/**
[""] *	@brief	Invoked whenever the central manager's state has been updated.
[""] *
[""] *	@param 	bleSupported 	Boolean
[""] *
[""] */
-(void)bleDidUpdateState:(BOOL)bleSupported;

/**
[""] *	@brief	Invoked whenever the Device has been Discovered.
[""] *
[""] *	@param 	dev 	DFBlunoDevice
[""] *
[""] */
-(void)didDiscoverDevice:(DFBlunoDevice*)dev;

/**
[""] *	@brief	Invoked whenever the Device is ready to communicate.
[""] *
[""] *	@param 	dev 	DFBlunoDevice
[""] *
[""] */
-(void)readyToCommunicate:(DFBlunoDevice*)dev;

/**
[""] *	@brief	Invoked whenever the Device has been disconnected.
[""] *
[""] *	@param 	dev 	DFBlunoDevice
[""] *
[""] */
-(void)didDisconnectDevice:(DFBlunoDevice*)dev;

/**
[""] *	@brief	Invoked whenever the data has been written to the BLE Device.
[""] *
[""] *	@param 	dev 	DFBlunoDevice
[""] *
[""] */
-(void)didWriteData:(DFBlunoDevice*)dev;

/**
[""] *	@brief	Invoked whenever the data has been received from the BLE Device.
[""] *
[""] *	@param 	data 	Data
[""] *	@param 	dev 	DFBlunoDevice
[""] *
[""] */
-(void)didReceiveData:(NSData*)data device:(DFBlunoDevice*)dev;


@end

@interface DFBlunoManager : NSObject<CBCentralManagerDelegate,CBPeripheralDelegate>

@property (nonatomic,weak) id<DFBlunoDelegate> delegate;
@property (nonatomic) BOOL runOnMainThread; // defaults to YES
@property (nonatomic, retain) NSString* terminator;
/**
[""] *	@brief	Singleton
[""] *
[""] *	@return	DFBlunoManager
[""] */
+ (instancetype)sharedInstance;

/**
[""] *	@brief	Scan the BLUNO device
[""] *
[""] */
- (void)scan;

/**
[""] *	@brief	Stop scanning
[""] *
[""] */
- (void)stop;

/**
[""] *	@brief	Clear the list of the discovered device
[""] *
[""] */
- (void)clear;

/**
[""] *	@brief	Connect to device
[""] *
[""] *	@param 	dev 	DFBlunoDevice
[""] *
[""] */
- (void)connectToDevice:(DFBlunoDevice*)dev;

/**
[""] *	@brief	Disconnect from the device
[""] *
[""] *	@param 	dev 	DFBlunoDevice
[""] *
[""] */
- (void)disconnectToDevice:(DFBlunoDevice*)dev;

/**
[""] *	@brief	Write the data to the device
[""] *
[""] *	@param 	data 	Daya
[""] *	@param 	dev 	DFBlunoDevice
[""] *
[""] */
- (void)writeDataToDevice:(NSData*)data Device:(DFBlunoDevice*)dev;

@end
