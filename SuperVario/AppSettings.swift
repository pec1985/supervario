//
//  AppSettings.swift
//  SuperVario
//
//  Created by Pedro Enrique on 4/10/17.
//  Copyright © 2017 Pedro Enrique. All rights reserved.
//

import Foundation
import UIKit

let AppSettingsNotification = Notification.Name("AppSettingsNotification")
class AppSettings
{
	private let SINK_THREASHOLD_VALUE = -250.0
	private let CLIMB_THREASHOLD_VALUE = 20.0
	private let BRIGHTNESS_VALUE = 80.0
	private let SHOW_COMPASS_VALUE = true
	private let SHOW_CLIMB_RATE_VALUE = true
	private let ANIMATIONS_ON_VALUE = true

	private static var Instance = AppSettings()
	static var AnimationsOn:Bool {
		get {
			return Instance.animationsOn
		}
		set {
			Instance.animationsOn = newValue
		}
	}
	static var ShowClimbRate:Bool {
		get {
			return Instance.showClimbRate
		}
		set {
			Instance.showClimbRate = newValue
		}
	}
	static var ShowCompass:Bool {
		get {
			return Instance.showCompass
		}
		set {
			Instance.showCompass = newValue
		}
	}
	static var Brightness:CGFloat {
		get {
			return Instance.brightness
		}
		set {
			Instance.brightness = newValue
		}
	}
	static var ClimbThreshold:Measurement<UnitSpeed> {
		get {
			return Instance.climbThreshold
		}
		set {
			Instance.climbThreshold = newValue
		}
	}
	static var SinkThreshold:Measurement<UnitSpeed> {
		get {
			return Instance.sinkThreshold
		}
		set {
			Instance.sinkThreshold = newValue
		}
	}
	static func Reset() {
		let instance = Instance;
		instance._animationsOn = nil
		instance._showClimbRate = nil
		instance._showCompass = nil
		instance._brightness = nil
		instance._climbThreshold = nil
		instance._sinkThreshold = nil
		
		let defaults = UserDefaults.standard
		defaults.removeObject(forKey:instance.SINK_THREASHOLD)
		defaults.removeObject(forKey:instance.CLIMB_THREASHOLD)
		defaults.removeObject(forKey:instance.BRIGHTNESS)
		defaults.removeObject(forKey:instance.SHOW_COMPASS)
		defaults.removeObject(forKey:instance.SHOW_CLIMB_RATE)
		defaults.removeObject(forKey:instance.ANIMATIONS_ON)
		
		instance.notifyApp()
	}

	private func notifyApp() {		
		NotificationCenter.default.post(Notification(name:AppSettingsNotification))
	}
	
	private func boolForValue (key: String, def: Bool) -> Bool {
		if let val = UserDefaults.standard.object(forKey: key) {
			return (val as? NSNumber)!.boolValue
		}
		UserDefaults.standard.set(def, forKey: key)
		return def
	}

	private func doubleForValue (key: String, def: Double) -> Double {
		if let val = UserDefaults.standard.object(forKey: key) {
			return (val as? NSNumber)!.doubleValue
		}
		UserDefaults.standard.set(def, forKey: key)
		return def
	}

	private let ANIMATIONS_ON = "animationsOn"
	private var _animationsOn:Bool?
	private var animationsOn:Bool {
		set {
			UserDefaults.standard.set(newValue, forKey: ANIMATIONS_ON)
			self._animationsOn = nil
			self.notifyApp()
		}
		get {
			if self._animationsOn == nil {
				self._animationsOn = self.boolForValue(key: ANIMATIONS_ON, def: ANIMATIONS_ON_VALUE)
			}
			return self._animationsOn!
		}
	}
	private let SHOW_CLIMB_RATE = "showClimbRate"
	private var _showClimbRate:Bool?
	private var showClimbRate:Bool {
		set {
			UserDefaults.standard.set(newValue, forKey: SHOW_CLIMB_RATE)
			self._showClimbRate = nil
			self.notifyApp()
		}
		get {
			if self._showClimbRate == nil {
				self._showClimbRate = self.boolForValue(key: SHOW_CLIMB_RATE, def: SHOW_CLIMB_RATE_VALUE)
			}
			return self._showClimbRate!
		}
	}
	
	private let SHOW_COMPASS = "showCompass"
	private var _showCompass:Bool?
	private var showCompass:Bool {
		set {
			UserDefaults.standard.set(newValue, forKey: SHOW_COMPASS)
			self._showCompass = nil
			self.notifyApp()
		}
		get {
			if self._showCompass == nil {
				self._showCompass = self.boolForValue(key: SHOW_COMPASS, def: SHOW_COMPASS_VALUE)
			}
			return self._showCompass!
		}
	}

	private let BRIGHTNESS = "brightness"
	private var _brightness:CGFloat?
	private var brightness:CGFloat {
		set {
			UserDefaults.standard.set(newValue, forKey: BRIGHTNESS)
			UIScreen.main.brightness = newValue
			self._brightness = nil
			self.notifyApp()
		}
		get {
			if self._brightness == nil {
				self._brightness = CGFloat(self.doubleForValue(key: BRIGHTNESS, def: BRIGHTNESS_VALUE))
				UIScreen.main.brightness = self._brightness!
			}
			return self._brightness!
		}
	}
	private let CLIMB_THREASHOLD = "climbThreshold"
	private var _climbThreshold:Measurement<UnitSpeed>?
	private var climbThreshold:Measurement<UnitSpeed> {
		set {
			let mps = newValue.converted(to: .metersPerSecond).value
			UserDefaults.standard.set(mps, forKey: CLIMB_THREASHOLD)
			self._climbThreshold = nil
			self.notifyApp()
		}
		get {
			if self._climbThreshold == nil {
				let fpm = Measurement(value: CLIMB_THREASHOLD_VALUE, unit: UnitSpeed.feetPerMinute)
				let mps = fpm.converted(to: .metersPerSecond)
				let doubleValue = self.doubleForValue(key: CLIMB_THREASHOLD, def: mps.value)
				self._climbThreshold = Measurement(value: doubleValue, unit: .metersPerSecond)
			}
			return self._climbThreshold!
		}
	}
	private let SINK_THREASHOLD = "sinkThreshold"
	private var _sinkThreshold:Measurement<UnitSpeed>?
	private var sinkThreshold:Measurement<UnitSpeed> {
		set {
			let mps = newValue.converted(to: .metersPerSecond).value
			let negativeValue = mps > 0 ? -mps : mps
			UserDefaults.standard.set(negativeValue, forKey: SINK_THREASHOLD)
			self._sinkThreshold = nil
			self.notifyApp()
		}
		get {
			if self._sinkThreshold == nil {
				let fpm = Measurement(value: SINK_THREASHOLD_VALUE, unit: UnitSpeed.feetPerMinute)
				let mps = fpm.converted(to: .metersPerSecond)
				let doubleValue = self.doubleForValue(key: SINK_THREASHOLD, def: mps.value)
				self._sinkThreshold = Measurement(value: doubleValue, unit: .metersPerSecond)
			}
			return self._sinkThreshold!
		}
	}
}
