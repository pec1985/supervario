//
//  SVTimeLabel.swift
//  SuperVario
//
//  Created by Pedro Enrique on 3/10/17.
//  Copyright © 2017 Pedro Enrique. All rights reserved.
//

import UIKit

@IBDesignable class SVTimeLabel: SVLabel {

    var timer:Timer?
    override func didLoad() {
        super.didLoad()
        self.backgroundColor = .black
        self.text = "12:12:12"
        self.textAlignment = .center
        self.timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { _ in
            self.timeUpdate()
        })
        self.timeUpdate()
    }
    
    func timeUpdate() {
        let date = Date()
        let components = Calendar.current.dateComponents([.hour, .minute, .second], from: date)
        let hour = Int(components.hour!).twoDigit()
        let minute = Int(components.minute!).twoDigit()
        let second = Int(components.second!).twoDigit()
        self.text = "\(hour):\(minute):\(second)"
    }
}
