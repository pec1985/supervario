//
//  CommunicationManager.swift
//  SuperVario
//
//  Created by Pedro Enrique on 4/3/17.
//  Copyright © 2017 Pedro Enrique. All rights reserved.
//

import CoreLocation

struct VariometerData {
    var igcLatitude = "0000000N"
    var igcLongitude = "00000000W"
    var heading:Double = 0
    var climbRate = Measurement(value: 0, unit: UnitSpeed.metersPerSecond)
    var baroAltitude = Measurement(value: 0, unit: UnitLength.meters)
    var gpsAltitude = Measurement(value: 0, unit: UnitLength.meters)
    var speed = Measurement(value: 0, unit: UnitSpeed.knots)
}

protocol CommunicationDelegate:class {
    func onVariometerDataUpdate(data:VariometerData)
    func onGPSAquired()
}

class CommunicationManager : NSObject
{
    static let Instance = CommunicationManager()

    private var fileRecorder = IGCFileRecorder.Instance
    private var varioData = VariometerData()
    private let superVario = Variometer.Instance
    private var gpsAquired = false
    private var lowestAltitudeAccuracy = 10.0
    private var messageFromBT = ""
	private var previousUIUpdate = arduino_millis()
	private var previousPressureUpdate = arduino_millis()
    private var altitudeAccuracy:Double = 5
	private var altitudeSetManually = false
	
    var delegates = Array<CommunicationDelegate>()
	
	override init() {
		super.init()
		NotificationCenter.default.addObserver(self, selector: #selector(onSettingsNotification(sender:)), name: AppSettingsNotification, object: nil)
		self.onSettingsNotification(sender: Notification(name: AppSettingsNotification))
	}
	
	func onSettingsNotification(sender: Notification) {
		self.superVario.climbThreshold = AppSettings.ClimbThreshold.value
		self.superVario.sinkThreshold = AppSettings.SinkThreshold.value
	}
    // Background thread from Bluetooth
    func update(message:String)
    {
        var incomingString = message
        if !incomingString.startsWith(">") {
			Logger.log("1 - \(#line) \(incomingString)")
            return
        }
        incomingString.remove(at: incomingString.startIndex)
        let newStartIndex = incomingString.firstIndexOf(">")
        let endIndex = incomingString.firstIndexOf("<")
        
        if endIndex == -1 {
			Logger.log("2 - \(#line) \(incomingString)")
            return
        }
        if newStartIndex != -1 && newStartIndex < endIndex {
			Logger.log("3 - \(#line) \(incomingString)")
            return
        }
        DispatchQueue.main.async {
            self.processLine(incomingString.substring(with: incomingString.range(0, endIndex)))
        }
    }
    
    // UI thread
    func processLine(_ line:String) {
        var value = line
        let firstChar = value.remove(at: value.startIndex)
        switch firstChar {
		case "A":
			let now = arduino_millis()
			var intervalSince = now - self.previousPressureUpdate
			if intervalSince > 19.0 {
				self.previousPressureUpdate = now
				self.superVario.update(pressure: value.asDouble())
				self.varioData.climbRate = Measurement(value: self.superVario.climbRate, unit: .metersPerSecond)
				self.varioData.baroAltitude = Measurement(value: self.superVario.altitude, unit: .meters)
				intervalSince = now - self.previousUIUpdate
				if intervalSince > 250.0 {
					self.previousUIUpdate = now
					for delegate in self.delegates {
						delegate.onVariometerDataUpdate(data:self.varioData)
					}
				}
				self.superVario.update(pressure: value.asDouble())
			}
			
		case "B":
			self.varioData.igcLatitude = value
		case "C":
			self.varioData.igcLongitude = value
		case "D":
			self.varioData.gpsAltitude =  Measurement(value: value.asDouble(), unit: UnitLength.meters)
		case "E":
			self.altitudeAccuracy = value.asDouble()
		case "F":
			self.varioData.speed = Measurement(value: value.asDouble(), unit: .knots)
		case "G":
			self.varioData.heading = value.asDouble()
			
			// "G" is the last one, do this here
			if !self.gpsAquired {
				let fixed = (self.varioData.igcLatitude != "0000000N" && self.varioData.igcLongitude != "00000000W")
				if fixed {
					self.gpsAquired = true
					for delegate in self.delegates {
						delegate.onGPSAquired()
					}
				}
			}
			if !self.fileRecorder.recording && !self.altitudeSetManually {
				if self.gpsAquired && self.altitudeAccuracy < self.lowestAltitudeAccuracy {
					self.superVario.altitude = self.varioData.gpsAltitude.value
					self.lowestAltitudeAccuracy = self.altitudeAccuracy
				}
			}
			return
		default: break;
        }
    }
	
	private var _altitude = Measurement(value: -1, unit: UnitLength.meters)
	var altitude: Measurement<UnitLength> {
		set {
			_altitude = newValue
			self.altitudeSetManually = true
			self.superVario.altitude = _altitude.converted(to: .meters).value
		}
		get {
			return _altitude
		}
	}
}
