//
//  PVBeeper.swift
//  SuperVario
//
//  Created by Pedro Enrique on 2/27/17.
//  Copyright © 2017 Pedro Enrique. All rights reserved.
//

import AudioToolbox

func arduino_tone(_ freq: Double) {
	if _enabled == false { return }
    if freq == 0 {
        if _toneUnit == nil { return }
        _theta = 0
        _frequency = 0
        return
    } else {
        _frequency = Float(freq)
    }
    if _toneUnit == nil {
        initializeEngine()
    }
}

func arduino_tone_teardown() {
	if let unit = _toneUnit {
		AudioOutputUnitStop(unit)
		AudioUnitUninitialize(unit)
		AudioComponentInstanceDispose(unit)
	}
}

func arduino_tone_disable() {
	_enabled = false
}

func arduino_tone_enable() {
	_enabled = true
}

private var _enabled = true

private var _theta: Float32 = 0
private let _sampleRate:Float32 = 44100

private var _frequency:Float32 = 0.0
private var _toneUnit:AudioComponentInstance?

private func initializeEngine() {
    //  First, we need to establish which Audio Unit we want.
    
    //  We start with its description, which is:
    var outputUnitDescription = AudioComponentDescription()
    outputUnitDescription.componentType         = kAudioUnitType_Output
    outputUnitDescription.componentSubType      = kAudioUnitSubType_RemoteIO
    outputUnitDescription.componentManufacturer = kAudioUnitManufacturer_Apple
    outputUnitDescription.componentFlags = 0
    outputUnitDescription.componentFlagsMask = 0

    //  Next, we get the first (and only) component corresponding to that description
    let outputComponent = AudioComponentFindNext(nil, &outputUnitDescription)!
    
    //  Now we can create an instance of that component, which will create an
    //  instance of the Audio Unit we're looking for (the default output)
    AudioComponentInstanceNew(outputComponent, &_toneUnit)
    AudioUnitInitialize(_toneUnit!)
    
    //  Next step is to tell our output unit which function we'd like it
    //  to call to get audio samples. We'll also pass in this pointer,
    //  which can be a pointer to anything you need to maintain state between
    //  render callbacks. We only need to point to a double which represents
    //  the current phase of the sine wave we're creating.
    var callbackInfo = AURenderCallbackStruct()
    callbackInfo.inputProc = renderCallback
    callbackInfo.inputProcRefCon = nil
    AudioUnitSetProperty(_toneUnit!, kAudioUnitProperty_SetRenderCallback, kAudioUnitScope_Input, 0, &callbackInfo, UInt32(MemoryLayout<AURenderCallbackStruct>.size))

    //  Next we'll tell the output unit what format our generated audio will
    //  be in. Generally speaking, you'll want to stick to sane formats, since
    //  the output unit won't accept every single possible stream format.
    //  Here, we're specifying floating point samples with a sample rate of
    //  44000 Hz in mono (i.e. 1 channel)
    var streamFormat = AudioStreamBasicDescription()
    streamFormat.mSampleRate       = Float64(_sampleRate)
    streamFormat.mFormatID         = kAudioFormatLinearPCM
    streamFormat.mFormatFlags      = kAudioFormatFlagsNativeFloatPacked | kAudioFormatFlagIsNonInterleaved
    streamFormat.mChannelsPerFrame = 1
    streamFormat.mFramesPerPacket  = 1
    streamFormat.mBitsPerChannel   = 4 * 8
    streamFormat.mBytesPerPacket   = 4
    streamFormat.mBytesPerFrame    = 4
    
    AudioUnitSetProperty(_toneUnit!, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Input, 0, &streamFormat, UInt32(MemoryLayout<AudioStreamBasicDescription>.size))
    
    var maxFPS:UInt32 = 4096
    AudioUnitSetProperty(_toneUnit!, kAudioUnitProperty_MaximumFramesPerSlice, kAudioUnitScope_Global, 0, &maxFPS, UInt32(MemoryLayout<UInt32>.size))

	AudioOutputUnitStart(_toneUnit!)
}

private let renderCallback: AURenderCallback = { (inRefCon, ioActionFlags, inTimeStamp, inBusNumber, inNumberFrames, ioData) -> OSStatus in
    
    // ioData is where we're supposed to put the audio samples we've created
    let inputDataPtr = UnsafeMutableAudioBufferListPointer(ioData!)
    let mBuffers = inputDataPtr[0]
    let outputBuffer = UnsafeMutableRawPointer(mBuffers.mData)!
    let dataArray = outputBuffer.assumingMemoryBound(to: Float32.self)

    
    let pi = Float32(Double.pi)
    var theta:Float32 = _theta
    let theta_increment:Float32 = 2.0 * pi * _frequency / _sampleRate

    for i in 0..<Int(inNumberFrames) {
        dataArray[i] = sin(theta)
        theta += theta_increment
        if (theta > 2.0 * pi)
        {
            theta -= 2.0 * pi
        }
    }
    _theta = theta
    
    return noErr
}
