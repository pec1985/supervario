////
////  PecVario.swift
////  SuperVario
////
////  Created by Pedro Enrique on 6/16/16.
////  Copyright © 2016 Pedro Enrique. All rights reserved.
////

import Foundation
import QuartzCore

fileprivate struct KalmanFilter
{
    public var x_abs:Double = 0;  // The absolute value of x.
    public var x_vel:Double = 0;  // The rate of change of x.
    
    // Covariance matrix for the state.
    private var p_abs_abs:Double = 0;
    private var p_abs_vel:Double = 0;
    private var p_vel_vel:Double = 0;
    
    // The variance of the acceleration noise input in the system model.
    public var var_accel:Double = 0;
    
    //calculation variables
    private var y:Double = 0;
    private var s_inv:Double = 0;
    private var k_abs:Double = 0;
    private var k_vel:Double = 0;
    
    public init(_ acceleration_noise:Double)
    {
        reset(acceleration_noise)
    }
    public mutating func reset(_ acceleration_noise:Double)
    {
        var_accel = acceleration_noise
        reset(0.0, 0.0)
    }
    public mutating func reset(_ abs_value:Double, _ vel_value:Double)
    {
        x_abs = abs_value;
        x_vel = vel_value;
        p_abs_abs = 1.0e10;
        p_abs_vel = 0.0;
        p_vel_vel = var_accel;
    }
    public mutating func update(_ z_abs:Double, _ var_z_abs:Double, _ dt:Double)
    {
        // Note: math is not optimized by hand. Let the compiler sort it out.
        // Predict step.
        // Update state estimate.
        x_abs += x_vel * dt;
        // Update state covariance. The last term mixes in acceleration noise.
        p_abs_abs += 2.0 * dt * p_abs_vel + dt * dt * p_vel_vel + var_accel * dt * dt * dt * dt / 4.0;
        p_abs_vel += dt * p_vel_vel + var_accel * dt * dt * dt / 2.0;
        p_vel_vel += var_accel * dt * dt;
        
        // Update step.
        y = z_abs - x_abs;  // Innovation.
        s_inv = 1 / (p_abs_abs + var_z_abs);  // Innovation precision.
        k_abs = p_abs_abs * s_inv;  // Kalman gain
        k_vel = p_abs_vel * s_inv;
        // Update state estimate.
        x_abs += k_abs * y;
        x_vel += k_vel * y;
        // Update state covariance.
        p_vel_vel -= p_abs_vel * k_vel;
        p_abs_vel -= p_abs_vel * k_abs;
        p_abs_abs -= p_abs_abs * k_abs;
    }
}

fileprivate struct Altimeter
{
    private var m_rawPressure:Double = 101325.0
    private var m_seaLevelPressure:Double = 101325.0
    private var m_dampedAltitude:Double = 0
    private var m_rawAltitude:Double = 0
    private var m_altDamp:Double = 0.05;
    private var m_positionNoise:Double = 0.2
    private var m_dampedAltStarted:Bool = false
    private var m_kalmanFilter = KalmanFilter(1.0)
    
    public init() {
        
    }
    public var seaLevelPressure:Double {
        set {
            m_rawPressure = newValue
            m_seaLevelPressure = newValue
            m_dampedAltStarted = false
        }
        get {
            return m_seaLevelPressure
        }
    }
    public var altDamp:Double {
        set {
            m_altDamp = newValue
        }
        get {
            return m_altDamp
        }
    }
    public var positionNoise:Double {
        set {
            m_positionNoise = newValue
        }
        get {
            return m_positionNoise
        }
    }
    public var altitude:Double {
        set {
            m_kalmanFilter.reset(1.0)
            m_dampedAltitude = newValue
            m_dampedAltStarted = true
            m_seaLevelPressure = m_rawPressure / pow(1.0 - (m_dampedAltitude / 44330.0), 5.255)
        }
        get {
            return m_kalmanFilter.x_abs
        }
    }
    public var varioValue:Double {
        return m_kalmanFilter.x_vel
    }
    
    public mutating func addPressure(_ pressure:Double, andTime time:Double) {
        m_rawPressure = pressure
        m_rawAltitude = 44330.0 * (1.0 - pow((m_rawPressure / m_seaLevelPressure), 0.190295))
        if (m_dampedAltStarted) {
            m_dampedAltitude = m_dampedAltitude + m_altDamp * (m_rawAltitude - m_dampedAltitude)
        } else {
            m_dampedAltitude = m_rawAltitude
            m_dampedAltStarted = true
        }
        m_kalmanFilter.update(m_dampedAltitude, m_positionNoise, time)
    }
}

fileprivate struct Point {
    var x:Double = 0
    var y:Double = 0
}
fileprivate struct PiecewiseLinearFunction
{
    var m_points = Array<Point>()
    var m_posInfValue:Double = 0x7ff0000000000000
    var m_negInfValue:Double = 0xfff0000000000000
    init() {
        
    }
    
    mutating func addPoint(_ point:Point) {
        if m_points.count == 0 {
            m_points.append(point)
        } else if point.x > m_points.last!.x {
            m_points.append(point)
        } else {
            for i in 0..<m_points.count {
                if m_points[i].x > point.x {
                    m_points[i] = point
                    return
                }
            }
        }
    }
    
    func getValue(_ x:Double) -> Double {
        if x == m_posInfValue {
            return m_posInfValue
        } else if x == m_negInfValue {
            return m_negInfValue
        } else if m_points.count == 1 {
            return m_points[0].y
        }
        var point: Point
        var lastPoint = m_points.last!
        if x <= lastPoint.y {
            return lastPoint.y
        }
        for i in 0..<m_points.count {
            point = m_points[i]
            if x <= point.x {
                let ratio = (x - lastPoint.x) / (point.x - lastPoint.x)
                return lastPoint.y + ratio * (point.y - lastPoint.y)
            }
            lastPoint = point
        }
        return lastPoint.y
    }
}

fileprivate struct BeeperController {
    var varioClimbThreshold:Double = 0.1 // 20FPM
    var varioClimbCutoff:Double = 0.1 // 20FPM
    var varioSinkThreshold:Double = -1.3208 // 260 FPM
    var varioSinkCutoff:Double = -1.3208 // 260FPM
    var variable:Double = 0
    var climbing:Bool = false
    var sinking:Bool = false
    var beeping:Bool = false
    var increment:Double = 100
    var base:Double = 1000
    var sinkBase:Double = 500
    var sinkIncrement:Double = 100
    var piecewise = PiecewiseLinearFunction()
    var duration:Double {
        get {
            return piecewise.getValue(self.variable) * 1000.0
        }
    }
    init() {
        self.piecewise.addPoint(Point(x:0.135, y: 0.4755))
        self.piecewise.addPoint(Point(x:0.441, y: 0.3619))
        self.piecewise.addPoint(Point(x:1.029, y: 0.2238))
        self.piecewise.addPoint(Point(x:1.559, y: 0.1565))
        self.piecewise.addPoint(Point(x:2.471, y: 0.0985))
        self.piecewise.addPoint(Point(x:3.571, y: 0.0741))
    }
    mutating func rateFromLiftTone() -> Double {
        let hZ = self.base + self.increment * self.variable;
        var rate = hZ / 1000.0;
        if (rate < 0.5) {
            rate = 0.5;
        } else if (rate > 2.0) {
            rate = 2.0;
        } else if (rate == 1.0) {
            rate = 1.0 + .leastNormalMagnitude;
        }
        return rate * 1300.0
    }
    mutating func rateFromSinkTone() -> Double {
        let hZ = self.sinkBase + self.sinkIncrement * self.variable;
        var rate = hZ / 500.0;
        if (rate < 0.5) {
            rate = 0.5;
        } else if (rate > 2.0) {
            rate = 2.0;
        } else if (rate == 1.0) {
            rate = 1.0 + .leastNormalMagnitude;
        }
        return (rate * 800.0);
    }

    mutating func update(_ newValue: Double)
    {
        self.variable = newValue;
        if (self.varioClimbCutoff > self.varioClimbThreshold) {
            self.varioClimbCutoff = self.varioClimbThreshold;
        }
        if (self.varioSinkCutoff < self.varioSinkThreshold) {
            self.varioSinkCutoff = self.varioSinkThreshold;
        }
        if (newValue > self.varioClimbCutoff) {
            self.climbing = true;
            self.sinking = false;
            if (newValue > self.varioClimbThreshold) {
                self.beeping = true;
            }
            return;
        }
        if (newValue < self.varioClimbCutoff && newValue > self.varioSinkCutoff) {
            self.beeping = false;
            self.climbing = false;
            self.sinking = false;
            return;
        }
        if (newValue < self.varioSinkCutoff) {
            self.climbing = false;
            self.sinking = true;
            if (newValue <= self.varioSinkThreshold) {
                self.beeping = true;
            }
            return;
        }
    }
}

class Variometer
{
    static let Instance = Variometer()

	var climbRate:Double = 0
	var beepsOnLift:Bool = true
	var beepsOnSink:Bool = true
	
	private var altimeter = Altimeter()
	private var beeperController = BeeperController()
    private var startedBeeping:Bool = false
    private var shouldStartBeeping:Bool = true
    private var shouldStopBeeping:Bool = false
    private var canStartBeepingAt: Double = 0
    private var stopBeepingAt: Double = 0
    private var currentTime = arduino_millis()
	private var _altitude:Double = 0
	var altitude:Double {
		get {
			return self._altitude
		}
		set {
			self.altimeter.altitude = newValue
		}
	}
	
	var climbThreshold:Double {
		set {
			self.beeperController.varioClimbThreshold = newValue
			self.beeperController.varioClimbCutoff = newValue
		}
		get {
			return self.beeperController.varioClimbCutoff
		}
	}
	var sinkThreshold:Double {
		set {
			self.beeperController.varioSinkThreshold = newValue
			self.beeperController.varioSinkCutoff = newValue
		}
		get {
			return self.beeperController.varioSinkCutoff
		}
	}
	
	func update(pressure: Double) {
        let timeElapsed = (arduino_millis() - self.currentTime) / 1000
        self.altimeter.addPressure(pressure, andTime: timeElapsed)
        self.climbRate = self.altimeter.varioValue;
        self._altitude = self.altimeter.altitude;
        self.beeperController.update(self.climbRate)
        self.updateBeeps()
        self.currentTime = arduino_millis()
    }
	
	func updateLiftForTesting(fpm:Measurement<UnitSpeed>) {
		
		if fpm.value == 0 {
			arduino_tone(0)
			return
		}
		self.beeperController.update(fpm.converted(to: .metersPerSecond).value)
		self.updateBeeps()

	}
	
    func updateBeeps() {
        if (self.beeperController.beeping) {
            let climbing = self.beeperController.climbing;
            let sinking = self.beeperController.sinking;
            if (climbing || sinking) {
                let duration = self.beeperController.duration;
                let now = arduino_millis()
                if (now >= self.stopBeepingAt && self.shouldStopBeeping)
				{
                    if (climbing) {
                        shouldBeep(climbing: false, frequency:0);
                    }
                    self.shouldStopBeeping = false;
                    self.shouldStartBeeping = true;
                    self.startedBeeping = false;
                    self.canStartBeepingAt = now + duration;
                }
                else if (now >= self.canStartBeepingAt && self.shouldStartBeeping)
				{
                    self.shouldStartBeeping = false;
                    self.startedBeeping = true;
                    self.stopBeepingAt = now + duration;
                    self.shouldStopBeeping = true;
                }
                if (self.startedBeeping)
				{
                    let freq = climbing ? self.beeperController.rateFromLiftTone() : self.beeperController.rateFromSinkTone();
                    shouldBeep(climbing:climbing, frequency: freq);
                }
            } else {
                shouldBeep(climbing: false, frequency:0);
            }
        } else {
            shouldBeep(climbing: false, frequency:0);
        }

    }
    
    func shouldBeep(climbing:Bool, frequency:Double)
    {
        if frequency == 0 {
			arduino_tone(0)
            return;
        }
        let hasSound = climbing ? self.beepsOnLift : self.beepsOnSink
        if hasSound {
			arduino_tone(frequency)
        } else {
			arduino_tone(0)
        }
    }
}
