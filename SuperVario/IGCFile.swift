//
//  IGCFile.swift
//  SuperVario
//
//  Created by Pedro Enrique on 3/17/17.
//  Copyright © 2017 Pedro Enrique. All rights reserved.
//

import Foundation
import CoreLocation

class IGCFile
{
    var maxAltitude: Measurement<UnitLength>?
    var minAltitude: Measurement<UnitLength>?
    var filePath = URL(fileURLWithPath: "")
    var fileName = String()
    var creationDate = Date()
    init(fileName: String) {
        self.fileName = fileName
        let documents = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        self.filePath = documents.appendingPathComponent(self.fileName)
        let attr = try! FileManager.default.attributesOfItem(atPath: self.filePath.path)
        self.creationDate = attr[.creationDate] as! Date
        let fileHandle = try! FileHandle(forReadingFrom: self.filePath)
        let count = Int(self.header.characters.count) + 1
        let header = String(data:fileHandle.readData(ofLength: count), encoding: .utf8)!
        self.header = header
        fileHandle.closeFile()
    }

	init(seconds:Int) {
        let documents = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        self.creationDate = Date().addingTimeInterval(TimeInterval(-seconds))
        self.fileName = IGCHelper.dateToFilename(date: self.creationDate)
        self.filePath = documents.appendingPathComponent(self.fileName)
        FileManager.default.createFile(atPath: self.filePath.path, contents: self.header.data(using: .utf8), attributes: nil)
    }

	deinit {
		self.releaseMemory()
	}
    private var _header:String?
    var header:String {
        get {
            if self._header == nil {
                self._header = "APEC002 SuperVario\r\n" +
                    "HFDTE\(IGCHelper.stringDate(date: self.creationDate))\r\n" +
                    "HFPLTPILOT:Pedro Enrique\r\n"
            }
            return self._header!
        }
        set {
            self._header = newValue
        }
    }
	
	func stopRecording() {
		let components = Calendar.current.dateComponents([.hour, .minute, .second], from: self.creationDate, to: Date())
		let hour = Int(components.hour!).twoDigit()
		let minute = Int(components.minute!).twoDigit()
		let second = Int(components.second!).twoDigit()
		self.writeLine("HTOTALTIME:\(hour)\(minute)\(second)\r\n")
	}
	
    func writeLine(_ str:String) {
        let fileHandle = try! FileHandle(forWritingTo: self.filePath)
        fileHandle.seekToEndOfFile()
        fileHandle.write(str.data(using: .utf8)!)
        fileHandle.closeFile()
    }
	
    func saveSentances(_ sentances:[IGCSentance]) {
        var str = "";
        for each in sentances {
            str.append(each.line)
        }
        self.writeLine(str)
    }
    
    func releaseMemory() {
        self._sentances = nil
    }
    
    private var _sentances:[IGCSentance]?
    var sentances:[IGCSentance] {
        get {
            if self._sentances == nil {
                var sentances = Array<IGCSentance>()
                let fileHandle = try! FileHandle(forReadingFrom: self.filePath)
                let stringContent = String(data:fileHandle.readDataToEndOfFile(), encoding: .utf8)!.replace("\r", withString: "")
                let lines = stringContent.components(separatedBy: "\n")
                fileHandle.closeFile()
                
                var maxAltitude:CLLocationDistance = 0
                var minAltitude:CLLocationDistance = 0
                var prev:IGCSentance?
                for line in lines where line.startsWith("B") {
                    var sentance = IGCSentance(line: line)
                    if let previous = prev {
                        if previous.location.equalTo(sentance.location) {
                            sentance.speed = previous.speed
                        } else {
							let loc1 = CLLocation(coordinate: CLLocationCoordinate2D(latitude: previous.location.latitude, longitude: previous.location.longitude),
							                      altitude: previous.gpsAltitude.converted(to: .meters).value,
							                      horizontalAccuracy: 0.0, verticalAccuracy: 0.0, course: 0.0,
							                      speed: 0.0, timestamp: Date()
							)
							let loc2 = CLLocation(coordinate: CLLocationCoordinate2D(latitude: sentance.location.latitude, longitude: sentance.location.longitude),
							                      altitude: sentance.gpsAltitude.converted(to: .meters).value,
							                      horizontalAccuracy: 0.0, verticalAccuracy: 0.0, course: 0.0,
							                      speed: 0.0, timestamp: Date()
							)
							sentance.speed = Measurement(value: loc1.distance(from: loc2), unit: UnitSpeed.metersPerSecond)
							sentance.climbRate = Measurement(value: loc2.altitude - loc1.altitude, unit: UnitSpeed.metersPerSecond)
                        }
                        maxAltitude = max(maxAltitude, sentance.gpsAltitude.value)
                        minAltitude = min(minAltitude, sentance.gpsAltitude.value)
                    } else {
                        maxAltitude = sentance.gpsAltitude.value
                        minAltitude = sentance.gpsAltitude.value
                    }
					sentances.append(sentance)
					prev = sentance
                }
                self.minAltitude = Measurement(value: minAltitude, unit: .meters)
                self.maxAltitude = Measurement(value: maxAltitude, unit: .meters)
                self._sentances = sentances
            }
            return self._sentances!
        }
    }

    private var _duration:Measurement<UnitDuration>?
    var duration:Measurement<UnitDuration>
    {
        get {
            if self._duration == nil {
                if let sentances = self._sentances {
                    self._duration = Measurement(value: Double(sentances.count), unit: .seconds)
                } else {
					do {
						let fileHandle = try FileHandle(forReadingFrom: self.filePath)
						fileHandle.seek(toFileOffset: fileHandle.seekToEndOfFile() - 18)
						if String(data:fileHandle.readData(ofLength: 10), encoding: .utf8)! == "TOTALTIME:" {
							let totalTime = String(data:fileHandle.readDataToEndOfFile(), encoding: .utf8)!.replace("\r", withString: "").replace("\n", withString: "")
							let hours = totalTime.substring(with: totalTime.range(0, 2))
							let minutes = totalTime.substring(with: totalTime.range(2, 2))
							let seconds = totalTime.substring(with: totalTime.range(4, 2))
							let h = Measurement(value: hours.asDouble(), unit: UnitDuration.hours)
							let m = Measurement(value: minutes.asDouble(), unit: UnitDuration.minutes)
							let s = Measurement(value: seconds.asDouble(), unit: UnitDuration.seconds)
							self._duration = h + m + s
						} else {
							let characters = Int(fileHandle.seekToEndOfFile()) - (20 + 13 + 26)
							self._duration = Measurement(value: Double(characters / 37), unit: .seconds)
						}
						fileHandle.closeFile()
					} catch {
						print(error)
					}
                }
            }
            return self._duration!
        }
    }
    
    private var _stringDuration:String?
    var stringDuration:String
    {
        get {
            if self._stringDuration == nil {
                self._stringDuration = IGCHelper.stringDuration(self.duration)
            }
            return self._stringDuration!
        }
    }
}
