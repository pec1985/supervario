//
//  IGCHelper.swift
//  SuperVario
//
//  Created by Pedro Enrique on 3/17/17.
//  Copyright © 2017 Pedro Enrique. All rights reserved.
//

import Foundation

class IGCHelper
{
    static var IGCalendar: Calendar {
        get {
            var calendar = Calendar.current
            calendar.timeZone = TimeZone(abbreviation: "UTC")!
            return calendar
        }
    }
    
    static func stringDate(date: Date) -> String {
        let components = IGCalendar.dateComponents([.year, .month, .day], from: date)
        let month = Int(components.month!).twoDigit()
        let day = Int(components.day!).twoDigit()
        let year = Int(components.year!).twoDigit()
        return "\(day)\(month)\(year)"
    }
    
    static func stringTime(date: Date) -> String{
        var components = IGCalendar.dateComponents([.hour, .minute, .second], from: date)
        let hour = Int(components.hour!).twoDigit()
        let minute = Int(components.minute!).twoDigit()
        let second = Int(components.second!).twoDigit()
        return "\(hour)\(minute)\(second)"
    }
    
    static func dateToFilename(date:Date) -> String {
        let components = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)
        let hour = Int(components.hour!).twoDigit()
        let minute = Int(components.minute!).twoDigit()
        let second = Int(components.second!).twoDigit()
        let month = Int(components.month!).twoDigit()
        let day = Int(components.day!).twoDigit()
        return "\(components.year!)-\(month)-\(day)_\(hour).\(minute).\(second).igc"
    }
        
    static func stringDuration(_ seconds: Measurement<UnitDuration>) -> String {
        let minutes = seconds.converted(to: UnitDuration.minutes)
        let hours = seconds.converted(to: UnitDuration.hours)
        let hoursValue = Int(hours.value.truncatingRemainder(dividingBy: 24.0))
        let minutesValue = Int(minutes.value.truncatingRemainder(dividingBy: 60.0))
        let secondsValue = Int(seconds.value.truncatingRemainder(dividingBy: 60.0))
        return "\(hoursValue.twoDigit()):\(minutesValue.twoDigit()):\(secondsValue.twoDigit())"
    }
}
