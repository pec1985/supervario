	//
	//  arduino_functions.c
	//  SuperVario
	//
	//  Created by Pedro Enrique on 4/5/17.
	//  Copyright © 2017 Pedro Enrique. All rights reserved.
	//

#import "arduino_functions.h"
#import <AudioToolbox/AudioToolbox.h>
#import <QuartzCore/QuartzCore.h>
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

#import <UIKit/UIKit.h>


#import <objc/runtime.h>

@implementation UIScreen (Tracking)

+ (void)load {
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		Class class = [self class];
		
		SEL originalSelector = @selector(setBrightness:);
		SEL swizzledSelector = @selector(xxx_setBrightness:);
		
		Method originalMethod = class_getInstanceMethod(class, originalSelector);
		Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector);
		
			// When swizzling a class method, use the following:
			// Class class = object_getClass((id)self);
			// ...
			// Method originalMethod = class_getClassMethod(class, originalSelector);
			// Method swizzledMethod = class_getClassMethod(class, swizzledSelector);
		
		BOOL didAddMethod =
		class_addMethod(class,
						originalSelector,
						method_getImplementation(swizzledMethod),
						method_getTypeEncoding(swizzledMethod));
		
		if (didAddMethod) {
			class_replaceMethod(class,
								swizzledSelector,
								method_getImplementation(originalMethod),
								method_getTypeEncoding(originalMethod));
		} else {
			method_exchangeImplementations(originalMethod, swizzledMethod);
		}
	});
}

#pragma mark - Method Swizzling

- (void)xxx_setBrightness:(CGFloat)value {
	[self xxx_setBrightness:value];
	NSLog(@"xxx_setBrightness: %f", value);
}

@end



@class SoundsListener;
static SoundsListener* _soundListenerInstance;
static BOOL _enabled = true;
static Float32 _theta = 0;
static Float32 _frequency = 0.0;
static Float32 _amplitude = 0.0;
static bool _shouldStop = false;

static AudioComponentInstance _toneUnit = NULL;

OSStatus renderCallback(void *inRefCon, AudioUnitRenderActionFlags *ioActionFlags, const AudioTimeStamp *inTimeStamp, UInt32 inBusNumber, UInt32 inNumberFrames, AudioBufferList *ioData);
void initializeEngine();
void arduino_tone_teardown();


@interface SoundsListener : NSObject { }
@end
@implementation SoundsListener
+(void)load
{
	_soundListenerInstance = [[SoundsListener alloc] init];
	[[NSNotificationCenter defaultCenter] addObserver:_soundListenerInstance selector:@selector(audioInterruption:) name:AVAudioSessionInterruptionNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:_soundListenerInstance selector:@selector(mediaServicesReset:) name:AVAudioSessionMediaServicesWereResetNotification object:nil];
}
-(void)audioInterruption:(NSNotification*)notification
{
	NSNumber *interruptionType = [[notification userInfo] objectForKey:AVAudioSessionInterruptionTypeKey];
	if (interruptionType != nil) {
		arduino_tone_teardown();
		switch ([interruptionType unsignedIntegerValue]) {
			case AVAudioSessionInterruptionTypeBegan:
				_enabled = false;
			break;
			case AVAudioSessionInterruptionTypeEnded:
				_enabled = true;
			break;
			default: break;
		}
	}
}
-(void)mediaServicesReset:(NSNotification*)notification
{
	_enabled = false;
	arduino_tone_teardown();
	_enabled = true;
}
@end


void arduino_tone(double freq)
{
	if (_enabled == false) { return; }
	if (freq == 0) {
		if (_toneUnit == NULL) { return; }
		_shouldStop = true;
		return;
	} else {
		if (_shouldStop) {
			_shouldStop = false;
			_theta = 0;
		}
		_frequency = (Float32)freq;
	}
	if (_toneUnit == NULL) {
		initializeEngine();
	}
}

void arduino_tone_teardown()
{
	if (_toneUnit != NULL) {
		AudioOutputUnitStop(_toneUnit);
		AudioUnitUninitialize(_toneUnit);
		AudioComponentInstanceDispose(_toneUnit);
		_toneUnit = NULL;
	}
}
void arduino_tone_disable()
{
	_enabled = false;
}
void arduino_tone_enable()
{
	_enabled = true;
}
double arduino_millis()
{
	return CACurrentMediaTime() * 1000.0;
}
void initializeEngine()
{
	AudioComponentDescription outputUnitDescription;
	outputUnitDescription.componentType         = kAudioUnitType_Output;
	outputUnitDescription.componentSubType      = kAudioUnitSubType_RemoteIO;
	outputUnitDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
	outputUnitDescription.componentFlags = 0;
	outputUnitDescription.componentFlagsMask = 0;
	
	AudioComponent outputComponent = AudioComponentFindNext(nil, &outputUnitDescription);
	
	AudioComponentInstanceNew(outputComponent, &_toneUnit);
	AudioUnitInitialize(_toneUnit);

	AURenderCallbackStruct callbackInfo;
	callbackInfo.inputProc = renderCallback;
	callbackInfo.inputProcRefCon = NULL;
	
	AudioStreamBasicDescription streamFormat;
	streamFormat.mSampleRate       = 44100;
	streamFormat.mFormatID         = kAudioFormatLinearPCM;
	streamFormat.mFormatFlags      = kAudioFormatFlagsNativeFloatPacked | kAudioFormatFlagIsNonInterleaved;
	streamFormat.mChannelsPerFrame = 1;
	streamFormat.mFramesPerPacket  = 1;
	streamFormat.mBitsPerChannel   = 4 * 8;
	streamFormat.mBytesPerPacket   = 4;
	streamFormat.mBytesPerFrame    = 4;
	UInt32 maxFPS = 4096;
	
	AudioUnitSetProperty(_toneUnit, kAudioUnitProperty_SetRenderCallback, kAudioUnitScope_Input, 0, &callbackInfo, sizeof(callbackInfo));
	AudioUnitSetProperty(_toneUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Input, 0, &streamFormat, sizeof(streamFormat));
	AudioUnitSetProperty(_toneUnit, kAudioUnitProperty_MaximumFramesPerSlice, kAudioUnitScope_Global, 0, &maxFPS, sizeof(UInt32));
	AudioOutputUnitStart(_toneUnit);
	
}
OSStatus renderCallback(void *inRefCon, AudioUnitRenderActionFlags *ioActionFlags, const AudioTimeStamp *inTimeStamp, UInt32 inBusNumber, UInt32 inNumberFrames, AudioBufferList *ioData)
{
	Float32 *outputBuffer = (Float32 *)ioData->mBuffers[0].mData;
	Float32 theta = _theta;
	Float32 theta_increment = 2.0 * M_PI * _frequency / 44100.0;
	Float32 amplitude = _amplitude;
	bool shouldStop = _shouldStop;
	
	for (UInt32 i = 0; i < inNumberFrames; i++)
	{
		if (shouldStop) {
			if (amplitude > 0.0) {
				amplitude -= 0.005;
			}
			if (amplitude < 0.0) {
				amplitude = 0.0;
			}
		} else {
			if (amplitude < 1.0) {
				amplitude += 0.005;
			}
			if (amplitude > 1.0) {
				amplitude = 1.0;
			}
		}
		outputBuffer[i] = sin(theta) * amplitude;
		theta += theta_increment;
		if (theta > 2.0 * M_PI) {
			theta -= 2.0 * M_PI;
		}
	}
	_theta = theta;
	_amplitude = amplitude;
	return noErr;
}
