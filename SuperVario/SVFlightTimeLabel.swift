//
//  SVFlightTimeLabel.swift
//  SuperVario
//
//  Created by Pedro Enrique on 3/10/17.
//  Copyright © 2017 Pedro Enrique. All rights reserved.
//

import UIKit

@IBDesignable class SVFlightTimeLabel: SVLabel {

    var timer:Timer?
    var startTime = Date()
    override func didLoad() {
        super.didLoad()
        self.text = "00:00:00"
    }
    
    func startCounting(seconds: Int) {
        self.text = "00:00:\(seconds.twoDigit())"
        self.startTime = Date().add(seconds: (-1 * seconds))
        self.timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { _ in
            self.timeUpdate()
        }
        self.timeUpdate()
    }
    
    func endCounting() {
        self.timer?.invalidate()
        self.timer = nil
    }
    
    private func timeUpdate() {
        let components = Calendar.current.dateComponents([.hour, .minute, .second], from: self.startTime, to: Date())
        let hour = Int(components.hour!).twoDigit()
        let minute = Int(components.minute!).twoDigit()
        let second = Int(components.second!).twoDigit()
        self.text = "\(hour):\(minute):\(second)"
    }

}
