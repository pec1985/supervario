//
//  SVSpeedLabel.swift
//  SuperVario
//
//  Created by Pedro Enrique on 2/28/17.
//  Copyright © 2017 Pedro Enrique. All rights reserved.
//

import UIKit

@IBDesignable class SVSpeedLabel: SVLabel
{
    private var speed:Double = 0
    override func didLoad() {
        super.didLoad()
        self.text = "12 mph"
    }
    override func updateData(data: VariometerData) {
        if data.speed.value == self.speed { return }
        self.speed = data.speed.value
        let mph = data.speed.converted(to: UnitSpeed.milesPerHour).value
        self.text = "\(Int(mph).commaSeparated()) mph"
    }

}
