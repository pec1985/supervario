//
//  SVProtocol.swift
//  SuperVario
//
//  Created by Pedro Enrique on 4/1/17.
//  Copyright © 2017 Pedro Enrique. All rights reserved.
//

protocol SVProtocol:class {
    func updateData(data: VariometerData)
}
