//
//  Logger.swift
//  SuperVario
//
//  Created by Pedro Enrique on 4/6/17.
//  Copyright © 2017 Pedro Enrique. All rights reserved.
//

import Foundation

class Logger
{
	static let Instance = Logger()
	private var filePath:URL!
	private var firstTime = true
	private init() {
		let documents = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
		self.filePath = documents.appendingPathComponent("logger.txt")
		if !FileManager.default.fileExists(atPath: self.filePath.path) {
			FileManager.default.createFile(atPath: self.filePath.path, contents: nil)
		}
	}
	
	private  func log(_ string: String)
	{
		var tmp = ""
		if self.firstTime {
			self.firstTime = false
			tmp = "=============" + IGCHelper.stringDate(date: Date()) + "=============\n"
		}
		
		var components = Calendar.current.dateComponents([.hour, .minute, .second], from: Date())
		let hour = Int(components.hour!).twoDigit()
		let minute = Int(components.minute!).twoDigit()
		let second = Int(components.second!).twoDigit()
		let time = "\(hour):\(minute):\(second)"

		let sentance = tmp + time + " " + string + "\n"
		let fileHandle = try! FileHandle(forWritingTo: self.filePath)
		fileHandle.seekToEndOfFile()
		fileHandle.write(sentance.data(using: .utf8)!)
		fileHandle.closeFile()
	}
	
	static func log(_ string: String) {
		Logger.Instance.log(string)
	}
}
