//
//  SVAltitudeLabel.swift
//  SuperVario
//
//  Created by Pedro Enrique on 2/27/17.
//  Copyright © 2017 Pedro Enrique. All rights reserved.
//

import UIKit

@IBDesignable class SVAltitudeLabel: SVLabel
{
    private var altitude:Double = 0
    override func didLoad() {
        super.didLoad()
        self.text = "5600 ft"
    }
    
    override func updateData(data: VariometerData)
    {
        if self.altitude == data.baroAltitude.value { return }
        self.altitude = data.baroAltitude.value
        let feet = data.baroAltitude.converted(to: .feet).value
        self.text = "\(Int(feet).commaSeparated()) ft"
    }
}
