//
//  SVCircleView.swift
//  SuperVario
//
//  Created by Pedro Enrique on 3/31/17.
//  Copyright © 2017 Pedro Enrique. All rights reserved.
//

import UIKit

@IBDesignable class SVCircleView : UIView, SVProtocol
{
    var color = UIColor.white
    var lineSpacing = 5
    
    func createLabel(text: String, angle:CGFloat, rotated:Bool) -> UILabel {
        let radius = self.bounds.width/2
        let center = self.bounds.center
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20)
        label.frame.origin = center.positionFrom(angle: (angle-90), radius: radius-10)
        label.text = text
        label.sizeToFit()
        label.frame.origin.x -= label.frame.size.width/2
        label.frame.origin.y -= label.frame.size.height/2
        if rotated {
            label.transform = CGAffineTransform(rotationAngle: .toRadians((angle)))
        }
        label.textColor = self.color
        return label
    }
    
    override func draw(_ rect: CGRect) {
        self.color.setStroke()
        var path:UIBezierPath
        let radius = rect.width/2
        let center = rect.center
        for i in 0..<360 {
            if i % self.lineSpacing == 0 {
                let length:CGFloat
                if i % 15 == 0 {
                    length = 50
                } else {
                    length = 40
                }
                path = UIBezierPath()
                path.move(to: center.positionFrom(angle: CGFloat(i), radius: radius-length))
                path.addLine(to: center.positionFrom(angle: CGFloat(i), radius: radius-20))
                path.lineWidth = 3
                path.stroke()
            }
        }
        
        super.draw(rect)
    }
    override func layoutSubviews() {
        self.backgroundColor = .clear
        super.layoutSubviews()
    }
    func updateData(data: VariometerData) {}
}
