//
//  SVBatteryLabel.swift
//  SuperVario
//
//  Created by Pedro Enrique on 3/7/17.
//  Copyright © 2017 Pedro Enrique. All rights reserved.
//

import UIKit

@IBDesignable class SVBatterLabel: SVLabel
{
    override func didLoad() {
        super.didLoad()
        self.backgroundColor = .black
        self.text = "100%"
        NotificationCenter.default.addObserver(self, selector: #selector(batteryUpdate), name: NSNotification.Name.UIDeviceBatteryLevelDidChange, object: nil)
        self.batteryUpdate()
        
    }
    
    func batteryUpdate() {
        if Thread.isMainThread {
            self.text = "\(Int(UIDevice.current.batteryLevel*100.0))%"
        } else {
            DispatchQueue.main.async {
                self.text = "\(Int(UIDevice.current.batteryLevel*100.0))%"
            }
        }
        Logger.log("BATTERY: \(self.text!)")
    }
}
