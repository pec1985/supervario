//
//  FlightDetailsViewController.swift
//  SuperVario
//
//  Created by Pedro Enrique on 3/21/17.
//  Copyright © 2017 Pedro Enrique. All rights reserved.
//

import UIKit
import Charts

class FlightDetailsViewController: UIViewController {

    @IBOutlet weak var speedChart: LineChartView!
    @IBOutlet weak var altitudeChart: LineChartView!
	@IBOutlet weak var climbChart: LineChartView!
    override func viewDidLoad() {
		self.view.transform = CGAffineTransform(rotationAngle: .toRadians(-90))
        super.viewDidLoad()
		
		var speedEntries = Array<ChartDataEntry>()
        var baroAltitudeEntries = Array<ChartDataEntry>()
		var gpsAltitudeEntries = Array<ChartDataEntry>()
		var climbEntries = Array<ChartDataEntry>()
		
		
		let width = Int(self.view.bounds.height)
		let count = self.file.sentances.count
		var div = Int(round(Double(count / width)))
		if div == 0 { div = 1 }
		for i in 0..<count where i % div == 0 {
			let value = self.file.sentances[i]
            speedEntries.append(ChartDataEntry(x:Double(i), y: value.speed.converted(to: .milesPerHour).value))
            baroAltitudeEntries.append(ChartDataEntry(x:Double(i), y: value.baroAltitude.converted(to: .feet).value))
            gpsAltitudeEntries.append(ChartDataEntry(x:Double(i), y: value.gpsAltitude.converted(to: .feet).value))
			climbEntries.append(ChartDataEntry(x:Double(i), y: value.climbRate.converted(to: .feetPerMinute).value))
        }
        
        let speedDataSet = LineChartDataSet(values: speedEntries, label: "Speed in MPH")
        speedDataSet.colors = [.red]
        speedDataSet.mode = .cubicBezier
        speedDataSet.drawCirclesEnabled = false
		
        let baroAltitudeDataSet = LineChartDataSet(values: baroAltitudeEntries, label: "Baro Altitude in Feet")
        baroAltitudeDataSet.colors = [.red]
        baroAltitudeDataSet.mode = .horizontalBezier
        baroAltitudeDataSet.drawCirclesEnabled = false
        baroAltitudeDataSet.valueFormatter = nil
        
		let gpsltitudeDataSet = LineChartDataSet(values: gpsAltitudeEntries, label: "GPS Altitude in Feet")
		gpsltitudeDataSet.colors = [.blue]
		gpsltitudeDataSet.mode = .horizontalBezier
		gpsltitudeDataSet.drawCirclesEnabled = false
		gpsltitudeDataSet.valueFormatter = nil

		let climbDataSet = LineChartDataSet(values: climbEntries, label: "Climb Rate in Feet Per Minute")
		climbDataSet.colors = [.red]
		climbDataSet.mode = .horizontalBezier
		climbDataSet.drawCirclesEnabled = false
		climbDataSet.valueFormatter = nil

        self.speedChart.dragEnabled = true
        self.speedChart.data = LineChartData(dataSet: speedDataSet)

		self.altitudeChart.dragEnabled = true
		self.altitudeChart.data = LineChartData(dataSets: [gpsltitudeDataSet, baroAltitudeDataSet])
		
		self.climbChart.dragEnabled = true
		self.climbChart.data = LineChartData(dataSet: climbDataSet)

		
    }
    private var _file: IGCFile?
    var file: IGCFile
    {
        set {
            self._file = newValue
        }
        
        get {
            return self._file!
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
