//
//  IGCFileRecorder.swift
//  SuperVario
//
//  Created by Pedro Enrique on 2/27/17.
//  Copyright © 2017 Pedro Enrique. All rights reserved.
//

import Foundation

fileprivate struct IGCMessageForQueue {
	init(_ sentance: IGCSentance, _ speed:Double) {
		let metersPerSecond = Measurement(value: speed, unit: UnitSpeed.metersPerSecond)
		let mph = metersPerSecond.converted(to: UnitSpeed.milesPerHour).value
		self.sentance = sentance
		self.speed = Int(mph)
	}
	var sentance:IGCSentance!
	var speed: Int!
}

protocol IGCFileRecorderDelegate:class {
    func igcFileRecorderStartedRecording(lines:Int)
    func igcFileRecorderStoppedRecording()
}

class IGCFileRecorder : CommunicationDelegate
{
    static let Instance = IGCFileRecorder()
	
    private var previousTimeStamp = ""
    private var filePath = ""
    private var messageQueue = Array<IGCMessageForQueue>()
    private let dispatchQueue = DispatchQueue(label: "SuperVario.igc.thread")
    var bufferTime = 10
    var minSpeed = 5
    var currentFile: IGCFile?
    var canStartRecording = false
    weak var delegate: IGCFileRecorderDelegate?
	
    var recording:Bool {
        get {
            return self.currentFile != nil
        }
    }
	
	func onVariometerDataUpdate(data:VariometerData)
	{
		if !self.canStartRecording { return }
		self.dispatchQueue.async {
			self.update(varioData:data, date: Date())
		}
	}
	
	func onGPSAquired() {
		self.canStartRecording = true
	}
	
    private func update(varioData: VariometerData, date:Date) {
        
        let timeStamp = IGCHelper.stringTime(date: date)
        if self.previousTimeStamp == timeStamp {
            return
        }
        self.previousTimeStamp = timeStamp
        let sentance = IGCSentance(varioData: varioData, timeStamp: timeStamp);
        let mph = varioData.speed.converted(to: .milesPerHour).value
        self.messageQueue.append(IGCMessageForQueue(sentance, mph))
        if self.messageQueue.count < self.bufferTime { return }
        if self.messageQueue.count > self.bufferTime { self.messageQueue.remove(at: 0) }
        var speedSum = 0
        for item in self.messageQueue {
            speedSum += item.speed
        }
        let average = speedSum / self.messageQueue.count
        if average < self.minSpeed {
            self.stopRecording()
            return
        }
        self.startRecording()
    }
    
    private func stopRecording() {
		self.currentFile?.stopRecording()
        self.currentFile = nil
        self.messageQueue.removeAll()
        DispatchQueue.main.async {
            self.delegate?.igcFileRecorderStoppedRecording()
        }
    }
    
    private func startRecording() {        
        if let currentFile = self.currentFile {
            currentFile.saveSentances([self.messageQueue.last!.sentance])
        } else {
            var insert = false
			var firstInserted = false
            var count = 0
            var sentances = Array<IGCSentance>()
            for (index, item) in self.messageQueue.enumerated() {
                if item.speed > 0 {
                    if index > 0 && firstInserted == false {
						firstInserted = true
						sentances.append(self.messageQueue[index-1].sentance)
                    }
                    insert = true
                }
                if insert {
					
					if item.sentance.isEqual(to: sentances.last) == false {
						sentances.append(item.sentance)
					}
                }
                count += 1
            }
			self.currentFile = IGCFile(seconds:count)
            self.currentFile!.saveSentances(sentances)
            DispatchQueue.main.async {
                self.delegate?.igcFileRecorderStartedRecording(lines: count)
            }
        }
    }
}
