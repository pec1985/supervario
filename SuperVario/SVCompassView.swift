//
//  CompassView.swift
//  SuperVario
//
//  Created by Pedro Enrique on 3/22/17.
//  Copyright © 2017 Pedro Enrique. All rights reserved.
//

import UIKit
import CoreLocation

@IBDesignable class SVCompassView: SVCircleView {
    
    private var prevAngle:CGFloat = 0
    private var viewsInit = false
    
    override func layoutSubviews()
    {
        if !self.viewsInit {
            self.viewsInit = true
            self.addSubview(self.createLabel(text: "N", angle: 0,   rotated: true))
            self.addSubview(self.createLabel(text: "NE",angle: 45,  rotated: true))
            self.addSubview(self.createLabel(text: "E", angle: 90,  rotated: true))
            self.addSubview(self.createLabel(text: "SE",angle: 135, rotated: true))
            self.addSubview(self.createLabel(text: "S", angle: 180, rotated: true))
            self.addSubview(self.createLabel(text: "SW",angle: 225, rotated: true))
            self.addSubview(self.createLabel(text: "W", angle: 270, rotated: true))
            self.addSubview(self.createLabel(text: "NW",angle: 315, rotated: true))
        }
        super.layoutSubviews()
    }
    
    override func updateData(data: VariometerData) {
		if self.isHidden { return }
        let angle = CGFloat(data.heading)
        if self.prevAngle == angle { return }
        self.prevAngle = angle
		if AppSettings.AnimationsOn {
			UIView.animate(withDuration: 0.25, delay: 0.0, options: [.curveLinear, .preferredFramesPerSecond30], animations: {
				self.transform = CGAffineTransform(rotationAngle: .toRadians(-angle))
			})
		} else {
			self.transform = CGAffineTransform(rotationAngle: .toRadians(-angle))
		}
    }
}
