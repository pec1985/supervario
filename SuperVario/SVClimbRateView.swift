//
//  ClimbRateView.swift
//  SuperVario
//
//  Created by Pedro Enrique on 3/31/17.
//  Copyright © 2017 Pedro Enrique. All rights reserved.
//

import UIKit

class SVNeedleView : UIView
{
    var color = UIColor.white
    override func draw(_ rect: CGRect) {
        self.color.setStroke()
        self.color.setFill()
        
        
        
        var circleRect = CGRect()
        circleRect.size.width = rect.width * 0.65
        circleRect.size.height = rect.height * 0.65
        circleRect.origin.x = (rect.width - circleRect.width) / 2
        circleRect.origin.y = (rect.height - circleRect.height) / 2
        
        let circle = UIBezierPath(ovalIn: circleRect)
        circle.lineWidth = 4
        circle.stroke()
        
        let needleRect = CGRect(x: 20, y: (rect.height/2)-2, width: (circleRect.origin.x)-20, height: 4)
        let needle = UIBezierPath(rect: needleRect)
        needle.fill()
        
        super.draw(rect)
    }
}

@IBDesignable class SVClimbRateView: SVCircleView
{
    private var climbRate:Double = 0
    private var viewsInit = false
    private var needle = SVNeedleView()
    override func layoutSubviews()
    {
        if !self.viewsInit {
            self.viewsInit = true
            self.backgroundColor = .clear
            
            self.addSubview(self.createLabel(text: "0", angle: 270, rotated: true))

            for i in 1..<8 {
                let angle: CGFloat = 270 + CGFloat(15 * i)
                let number = "\(i * 200)"
                self.addSubview(self.createLabel(text: number, angle: angle, rotated: true))
            }

            for i in 1..<8 {
                let angle: CGFloat = 270 - CGFloat(15 * i)
                let number = "\(i * 200)"
                self.addSubview(self.createLabel(text: number, angle: angle, rotated: true))
            }

            self.needle.frame = self.bounds
            self.needle.backgroundColor = .clear
            self.addSubview(self.needle)
        }
    }
    
    override func updateData(data: VariometerData)
    {
		if self.isHidden { return }
        let climbRate = data.climbRate.value
        if climbRate == self.climbRate { return }
        self.climbRate = climbRate

        let feetPerMinute = data.climbRate.converted(to: .feetPerMinute).value
        let number:Double
        if feetPerMinute < 0 {
            number = max(feetPerMinute, -1200)
        } else {
            number = min(feetPerMinute, 1200)
        }
        let angle = number * 90 / 1200
		if AppSettings.AnimationsOn {
			UIView.animate(withDuration: 0.25, delay: 0.0, options: [.curveLinear, .preferredFramesPerSecond30], animations: {
				self.needle.transform = CGAffineTransform(rotationAngle: .toRadians(CGFloat(angle)))
			})
		} else {
			self.needle.transform = CGAffineTransform(rotationAngle: .toRadians(CGFloat(angle)))
		}
    }
}
