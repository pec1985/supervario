//
//  SettingsTableViewController.swift
//  SuperVario
//
//  Created by Pedro Enrique on 4/6/17.
//  Copyright © 2017 Pedro Enrique. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController
{
	@IBOutlet weak var animationsSwitch: UISwitch!
	@IBOutlet weak var climbRateSwitch: UISwitch!
	@IBOutlet weak var compassSwitch: UISwitch!
	@IBOutlet weak var climbingField: UITextField!
	@IBOutlet weak var sinkingField: UITextField!

	deinit {
		self.testingTimer?.invalidate()
		Variometer.Instance.updateLiftForTesting(fpm: Measurement(value: 0, unit: .feetPerMinute))
	}
	
	override func viewDidLoad() {
        super.viewDidLoad()
		self.reloadData()
    }
	
	func reloadData() {
		self.animationsSwitch.isOn = AppSettings.AnimationsOn
		self.climbRateSwitch.isOn = AppSettings.ShowClimbRate
		self.compassSwitch.isOn = AppSettings.ShowCompass
		
		let fpmClimb = AppSettings.ClimbThreshold.converted(to: .feetPerMinute)
		let fpmSink = AppSettings.SinkThreshold.converted(to: .feetPerMinute)
		
		self.climbingField.text = String(format: "%.2f", fpmClimb.value)
		self.sinkingField.text = String(format: "%.2f", fpmSink.value)
		self.tableView!.reloadData()
	}
	@IBAction func onAnimationsSwitch(_ sender: UISwitch) {
		AppSettings.AnimationsOn = sender.isOn
	}
	@IBAction func onClimbRateSwitch(_ sender: UISwitch) {
		AppSettings.ShowClimbRate = sender.isOn
	}
	@IBAction func onCompassSwitch(_ sender: UISwitch) {
		AppSettings.ShowCompass = sender.isOn
	}
	@IBAction func onClimbField(_ sender: UITextField) {
		if let text = sender.text {
			AppSettings.ClimbThreshold = Measurement(value: text.asDouble(), unit: .feetPerMinute)
		}
	}
	@IBAction func onSinkField(_ sender: UITextField) {
		if let text = sender.text {
			AppSettings.SinkThreshold = Measurement(value: text.asDouble(), unit: .feetPerMinute)
		}
	}
	@IBAction func onCloseButton(_ sender: Any) {
		self.navigationController!.dismiss(animated: true, completion: {})
	}
	
	@IBAction func onFieldEnter(_ sender: UITextField) {
		sender.resignFirstResponder()
	}
	@IBAction func onResetButton(_ sender: Any) {
		AppSettings.Reset()
		self.reloadData()
	}
	@IBAction func onKeyboardDown(_ sender: Any) {
		self.climbingField.resignFirstResponder()
		self.sinkingField.resignFirstResponder()
	}
	
	private var testingTimer:Timer?
	private var fpm:Double = 100;
	
	@IBAction func startStopTesting(_ sender: UIButton) {
		if self.testingTimer == nil {
			sender.setTitle("Stop Testing", for: .normal)
			self.testingTimer = Timer(fire: Date(), interval: 0.02, repeats: true, block: { _ in
				Variometer.Instance.updateLiftForTesting(fpm: Measurement(value: self.fpm, unit: .feetPerMinute))
			})
			RunLoop.current.add(self.testingTimer!, forMode: .commonModes)
		} else {
			sender.setTitle("Start Testing", for: .normal)
			self.testingTimer?.invalidate()
			self.testingTimer = nil
			Variometer.Instance.updateLiftForTesting(fpm: Measurement(value: 0, unit: .feetPerMinute))
		}
	}

	@IBAction func changeTestingValue(_ sender: UISlider) {
		self.fpm = Double(sender.value)
	}
}
