//
//  MapViewController.swift
//  SuperVario
//
//  Created by Pedro Enrique on 3/15/17.
//  Copyright © 2017 Pedro Enrique. All rights reserved.
//

import UIKit
import MapKit

class PEMKPolyline : MKPolyline
{
	var climbRate:Measurement<UnitSpeed>?
}

class MapViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var maxLabel: UILabel!
    @IBOutlet weak var minLabel: UILabel!

	private var coords = Array<CLLocationCoordinate2D>()
    var file: IGCFile!
	deinit {
		self.file.releaseMemory()
	}
    override func viewDidLoad() {
        super.viewDidLoad()
		
		var sents = [IGCSentance]()
        for sentance in self.file.sentances {
			self.coords.append(sentance.location)
			sents.append(sentance)
			if sents.count == 2 {
				let coords:[CLLocationCoordinate2D] = [
					sents[0].location,
					sents[1].location
				]
				
				let polyline = PEMKPolyline(coordinates: UnsafePointer<CLLocationCoordinate2D>(coords), count: 2)
				
				polyline.climbRate = sents[1].climbRate;
				
				self.mapView.add(polyline)
				sents.remove(at: 0)
			}
		}
        
        let maxFeet = self.file.maxAltitude?.converted(to: .feet).value
        let minFeet = self.file.minAltitude?.converted(to: .feet).value
        
        self.maxLabel.text = "Max Altitude: \(Int(maxFeet!))"
        self.minLabel.text = "Min Altitude: \(Int(minFeet!))"
        let polyline = MKPolyline(coordinates: UnsafePointer<CLLocationCoordinate2D>(self.coords), count: self.coords.count);
		
        self.mapView.add(polyline);
        self.mapView.showsUserLocation = true
        self.mapView.userTrackingMode = .none
        self.mapView.mapType = .satelliteFlyover
        self.mapView.centerCoordinate = self.coords[0];
        self.mapView.delegate = self;
        self.mapView.setVisibleMapRect(polyline.boundingMapRect, edgePadding: UIEdgeInsets(top:20,left:20,bottom:20,right:20), animated: false)
		self.mapView.remove(polyline)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
    }
	
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
	}
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer
    {
		if let polyline = overlay as? PEMKPolyline {
			let renderer = MKPolylineRenderer(polyline: polyline)
			renderer.lineWidth = 3.0
			renderer.strokeColor = UIColor.climpRate(climbRate: polyline.climbRate!)
			renderer.alpha = 1
			return renderer;
		}
		return MKPolylineRenderer(polyline: overlay as! MKPolyline)

    }
    
    @IBAction func closeButton(_ sender: Any) {
        self.navigationController!.dismiss(animated: true, completion: {})
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let details = segue.destination as! FlightDetailsViewController
        details.file = self.file
    }
    
}
