//
//  SVLabel.swift
//  SuperVario
//
//  Created by Pedro Enrique on 2/27/17.
//  Copyright © 2017 Pedro Enrique. All rights reserved.
//

import UIKit

@IBDesignable class SVLabel: UILabel, SVProtocol
{
    override init(frame: CGRect) {
        super.init(frame: frame)
        didLoad()
    }
		
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        didLoad()
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
        
    func didLoad() {
        self.textAlignment = .right
        self.font = UIFont.monospacedDigitSystemFont(ofSize: self.font.pointSize, weight: UIFontWeightRegular)
        self.textColor = .white
        self.backgroundColor = .clear
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        didLoad()
    }
    
    func updateData(data: VariometerData) { }

}
