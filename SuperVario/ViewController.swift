//
//  ViewController.swift
//  SuperVario
//
//  Created by Pedro Enrique on 2/27/17.
//  Copyright © 2017 Pedro Enrique. All rights reserved.
//

import UIKit
import MediaPlayer

class ViewController: UIViewController, UIViewControllerPreviewingDelegate, CommunicationDelegate, IGCFileRecorderDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var flightTimeLabel: SVFlightTimeLabel!
    @IBOutlet weak var altitudeLabel: SVAltitudeLabel!
    @IBOutlet weak var speedLabel: SVSpeedLabel!
    @IBOutlet weak var timeLabel: SVLabel!
    @IBOutlet weak var batteryLabel: SVLabel!
    @IBOutlet weak var recordingDot: UIView!
    @IBOutlet weak var noGPSLabel: UILabel!
    @IBOutlet weak var compassView: SVCompassView!
	@IBOutlet weak var compassIndicator: UIView!
    @IBOutlet weak var climbRateView: SVClimbRateView!
    @IBOutlet weak var volumeSlider:  MPVolumeView!
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var filesTableView: UITableView!

    private var fileRecorder = IGCFileRecorder.Instance
	private var files = Array<Array<IGCFile>>()
	private var totalTimes = Array<Double>()
    private var recording = false
    private var totalTime = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
		CommunicationManager.Instance.delegates.append(self)
        self.view.backgroundColor = .black
        self.filesTableView.dataSource = self
        self.filesTableView.delegate = self
        self.filesTableView.rowHeight = 60
        self.recordingDot.layer.cornerRadius = 15
        self.recordingDot.isHidden = true
        self.fileRecorder.delegate = self
        self.volumeSlider.showsRouteButton = false
		
		let navBar = self.navigationBar.topItem!
		navBar.title = "Log Book"
		navBar.rightBarButtonItem = UIBarButtonItem(title: "Edit",
		                                            style: .plain,
		                                            target: self,
		                                            action: #selector(editTableView(sender:))
		);
		navBar.leftBarButtonItem = UIBarButtonItem(title: "Settings",
		                                           style: .plain,
		                                           target: self,
		                                           action: #selector(onSettingsClick(sender:))
		);
		self.altitudeLabel.isUserInteractionEnabled = true
		self.registerForPreviewing(with: self, sourceView: self.altitudeLabel);
		self.filesTableView.tableFooterView = UILabel()
		self.filesTableView.tableFooterView!.frame.size.height = 50;
        self.reloadTableView()
    }

    override func viewWillAppear(_ animated: Bool) {
		self.compassView.isHidden = !AppSettings.ShowCompass
		self.compassIndicator.isHidden = !AppSettings.ShowCompass
		self.climbRateView.isHidden = !AppSettings.ShowClimbRate
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
	func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController?
	{
		let newViewController = UIViewController()
		let newLabel = SVAltitudeLabel()
		newLabel.font = self.altitudeLabel.font
		newLabel.frame = self.altitudeLabel.bounds
		newViewController.view.backgroundColor = self.view.backgroundColor
		newViewController.view.addSubview(newLabel)
		newViewController.preferredContentSize = self.altitudeLabel.frame.size
		return newViewController
	}
	
	func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController)
	{

		let alert = UIAlertController(title: "Set Altitude", message: nil, preferredStyle: .alert);
		let okButton = UIAlertAction(title: "Ok", style: .default, handler: { _ in
			let textField = alert.textFields!.first!
			if let text = textField.text {
				let feet = text.asDouble()
				CommunicationManager.Instance.altitude = Measurement(value:feet, unit: .feet)
				self.altitudeLabel.text = "\(Int(feet).commaSeparated()) ft"

			}
		})
		let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
		alert.addTextField { textField in
			if let string = self.altitudeLabel.text {
				// remove all non-numerical characters
				let range = string.range(of: "\\d+(\\.\\d*)?", options: .regularExpression)!
				textField.text = string.substring(with: range)
			}
			textField.clearButtonMode = .always
			textField.keyboardType = .numberPad
			textField.keyboardAppearance = .dark
		}
		
		alert.addAction(okButton);
		alert.addAction(cancelButton)

		self.present(alert, animated: true, completion: nil)
	}

	override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func reloadTableView() {
        self.files.removeAll()

		let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let content = try! FileManager.default.contentsOfDirectory(at: path, includingPropertiesForKeys: nil, options: [])
        var files = Array<IGCFile>()
        for each in content where each.lastPathComponent.lowercased().endsWith("igc") {
            files.append(IGCFile(fileName:each.lastPathComponent))
        }
        files = files.sorted(by: { (a, b) -> Bool in
            return a.creationDate.timeIntervalSinceReferenceDate > b.creationDate.timeIntervalSinceReferenceDate
        })
		
		var current:Int = 0
		for file in files {
			let year = Calendar.current.component(.year, from: file.creationDate)
			if year != current {
				self.files.append(Array<IGCFile>())
			}
			var arr = self.files.last!
			arr.append(file)
			self.files.removeLast()
			self.files.append(arr)
			current = year
		}
		self.calculateTotalTime()
        self.filesTableView.reloadData()
    }
    
	func calculateTotalTime() {
        var totalTime: Double = 0
        for (index, files) in self.files.enumerated() {
			for file in files {
				if self.totalTimes.count < (index+1) {
					self.totalTimes.append(0)
				}
				self.totalTimes[index] += file.duration.value
				totalTime += file.duration.value
			}
        }
        self.totalTime = IGCHelper.stringDuration(Measurement(value: totalTime, unit: .seconds))
		(self.filesTableView.tableFooterView as! UILabel).text = "Total Time: \(self.totalTime)"
		(self.filesTableView.tableFooterView as! UILabel).textAlignment = .center
    }
    
    func igcFileRecorderStartedRecording(lines: Int)
    {
        self.flightTimeLabel.startCounting(seconds: lines)
        self.recordingDot.isHidden = false
        self.scrollview.setContentOffset(CGPoint(x:0, y:0), animated: true)
        self.scrollview.isScrollEnabled = false
    }
    
    func igcFileRecorderStoppedRecording()
    {
        self.flightTimeLabel.endCounting()
        self.recordingDot.isHidden = true
        self.scrollview.isScrollEnabled = true
        self.reloadTableView()
    }

    func onVariometerDataUpdate(data: VariometerData)
    {
        self.altitudeLabel.updateData(data: data)
        self.speedLabel.updateData(data: data)
        self.compassView.updateData(data: data)
        self.climbRateView.updateData(data: data)
    }
    
    func onGPSAquired()
    {
        self.noGPSLabel.isHidden = true
    }
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return self.files.count
	}
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.files[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
        }
        
        let file = self.files[indexPath.section][indexPath.row];
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .short
        cell!.textLabel?.text = dateFormatter.string(from: file.creationDate)
        cell!.detailTextLabel?.text = "Flight duration: " + file.stringDuration
        if tableView.isEditing {
            cell!.backgroundColor = UIColor(white: 0.90, alpha: 1.0)
            cell!.accessoryType = .none
        } else {
            cell!.backgroundColor = .white
            cell!.accessoryType = .disclosureIndicator
        }
        return cell!
    }
	
	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		let file = self.files[section][0]
		let year = Calendar.current.component(.year, from: file.creationDate)
		let total = Measurement(value: self.totalTimes[section], unit: UnitDuration.seconds)
		let str = IGCHelper.stringDuration(total)
		return "\(year) Total time: \(str)"
	}
	
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "FlightDetailsNavigationController") as! UINavigationController
        let mapView = nav.viewControllers[0] as! MapViewController
        mapView.file = self.files[indexPath.section][indexPath.row];
		// IGCAnalizeFile(self.files[indexPath.row].filePath)		
        self.present(nav, animated: true, completion: {})
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        let file = self.files[indexPath.section][indexPath.row]
        return file.duration.converted(to: .minutes).value < 2 // 2 minutes
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle
    {
        return .delete
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        var arr = self.files[indexPath.section]
		let file = arr[indexPath.row]
		arr.remove(at: indexPath.row)
		
		self.files.remove(at: indexPath.section)
		self.files.insert(arr, at: indexPath.section)
		
        self.calculateTotalTime()
        tableView.reloadSections([0], with: .fade)
        let documents = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        try! FileManager.default.removeItem(at: documents.appendingPathComponent(file.fileName))
    }
	
	func onSettingsClick(sender: UIBarButtonItem) {
		let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingsNavigationController")
		self.present(vc!, animated: true, completion: {})
	}
	
    func editTableView(sender: UIBarButtonItem) {
        let editing = self.filesTableView.isEditing
        if editing {
            sender.title = "Edit"
        } else {
            sender.title = "Done"
        }
        self.filesTableView.isEditing = !editing
        self.filesTableView.reloadData()
    }
}

