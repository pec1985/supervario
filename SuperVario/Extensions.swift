import Foundation
import UIKit
import CoreGraphics
import CoreLocation

extension CLLocationCoordinate2D {
    func equalTo(_ loc2: CLLocationCoordinate2D) -> Bool {
        return self.longitude == loc2.longitude && self.latitude == loc2.latitude
    }
}
extension UnitSpeed {
    open class var feetPerMinute: UnitSpeed {
        get {
            return UnitSpeed(symbol: "ft/m", converter: UnitConverterLinear(coefficient: 0.00508, constant: 0.0))
        }
    }
}

extension CGPoint
{
    func positionFrom(angle: CGFloat, radius:CGFloat) -> CGPoint {
        return CGPoint(x:self.x + radius * cos(.toRadians(angle)), y:self.y + radius * sin(.toRadians(angle)))
    }
}
extension CGRect
{
    var center:CGPoint {
        get {
            return CGPoint(x: self.width/2, y: self.height/2)
        }
    }
}

extension Int
{
    func commaSeparated() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        return numberFormatter.string(from: NSNumber(value: self))!
    }
    
    func twoDigit() -> String {
        var temp = self
        while temp > 100 {
            temp -= 100
        }
        return temp < 10 ? "0\(temp)" : "\(temp)"
    }

}

extension CGFloat {
    public static func toRadians(_ num: CGFloat) -> CGFloat {
        return num * .pi / 180.0
    }
}

extension UIColor
{
	class func climpRate(climbRate: Measurement<UnitSpeed>) -> UIColor
	{
		let fpm = climbRate.converted(to: .feetPerMinute).value
		let value:Double
		let max:Double = 600
		let min:Double = -600
		if fpm > 0 {
			value = Double.minimum(max, fpm) / (max * 2)
		} else {
			value = Double.maximum(min, fpm) / -(min * 2)
		}
		let gradient = 0.50 + value
		let rgba = bilinear_gradient(gradient)
		return UIColor(red: CGFloat(rgba.r), green: CGFloat(rgba.g), blue: CGFloat(rgba.b), alpha: CGFloat(rgba.a))
	}
}

extension String
{
    func contains(_ s: String) -> Bool {
        return (self.range(of: s) != nil) ? true : false
    }
    func firstIndexOf(_ s:Character) -> Int {
        if let idx = self.characters.index(of: s) {
            return self.characters.distance(from: self.startIndex, to: idx)
        }
        return -1
    }
    func replace(_ target: String, withString: String) -> String
    {
        return self.replacingOccurrences(of: target, with: withString, options: .literal, range: nil)
    }
    func parts(_ separator:String) -> Array<String>
    {
        return (self as NSString).parts(separator)
    }
    func asDouble() -> Double {
        return (self as NSString).asDouble()
    }
    func asInt() -> Int {
        return (self as NSString).integerValue
    }
    func asBool() -> Bool {
        return (self as NSString).boolValue
    }
    func startsWith(_ str:String) -> Bool {
        return (self as NSString).hasPrefix(str)
    }
    func endsWith(_ str:String) -> Bool {
        return (self as NSString).hasSuffix(str)
    }
    var firstChar:String {
        return String((self as NSString).character(at: 0))
    }
    
    func range(_ a:Int, _ b:Int) -> Range<String.Index> {
        let start = self.index(self.startIndex, offsetBy: a)
        let end = self.index(start, offsetBy: b)
        return start..<end
        
    }
}

extension NSString
{
    func parts(_ separator:String = "*") -> Array<String>
    {
        return self.components(separatedBy: separator)
    }
    func asDouble() -> Double {
        var newStr = self.replacingOccurrences(of: "\r", with: "")
        newStr = self.replacingOccurrences(of: "\n", with: "")
        return (newStr as NSString).doubleValue
    }
    func asBool() -> Bool {
        var newStr = self.replacingOccurrences(of: "\r", with: "")
        newStr = self.replacingOccurrences(of: "\n", with: "")
        return (newStr as NSString).boolValue
    }
    var firstChar:unichar {
        return self.character(at: 0)
    }
}

extension Date {
    func add(seconds: Int) -> Date {
        return Calendar.current.date(byAdding: .second, value: seconds, to: self)!
    }
}
