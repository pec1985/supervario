//
//  color.swift
//  SuperVario
//
//  Created by Pedro Enrique on 4/23/17.
//  Copyright © 2017 Pedro Enrique. All rights reserved.
//

import Foundation

struct r_g_b_a {
	init(_ r:Double, _ g:Double, _ b:Double, _ a:Double) {
		self.r = r
		self.g = g
		self.b = b
		self.a = a
	}
	var r:Double = 0
	var g:Double = 0
	var b:Double = 0
	var a:Double = 0
}

struct h_s_l {
	init(_ h:Double, _ s:Double, _ l:Double) {
		self.h = h
		self.s = s
		self.l = l
	}
	var h:Double = 0
	var s:Double = 0
	var l:Double = 0
}

func h_to_value(_ p:Double, _ q:Double, _ _t:Double) ->Double {
	var t = _t
	if t < 0.0 {
		t += 1.0
	} else if 1.0 < t {
		t -= 1.0
	}
	if t < 1.0 / 6.0 {
		return p + 6.0 * (q - p) * t
	}
	else if t < 0.5 {
		return q
	}
	else if t < 2.0 / 3.0 {
		return p + 6.0 * (q - p) * (2.0 / 3.0 - t)
	}
	return p
}

func hsl_to_rgba(_ hsl:h_s_l, _ a:Double = 1.0) -> r_g_b_a
{
	let	h:Double = hsl.h
	let s:Double = hsl.s
	let l:Double = hsl.l
	var q:Double = 0
	if s == 0 {
		return r_g_b_a(l, l, l, a)
	}
	if l < 0.5 {
		q = l * (s + 1.0)
	} else {
		q = l + s - l * s
	}
	let p = 2.0 * l - q
	let r = h_to_value(p, q, h + 1.0 / 3.0)
	let g = h_to_value(p, q, h)
	let b = h_to_value(p, q, h - 1.0 / 3.0)
	return r_g_b_a(r, g, b, a)
}

func bilinear_gradient(_ value:Double) -> r_g_b_a
{
	var h:Double = 0
	if value < 0.0 {
		h = 2.0 / 3.0
	}
	else if value < 0.5 {
		h = (6.0 - 4.0 * value) / 9.0
	} else if value == 0.5 {
		h = 1.0 / 3.0
	} else if value < 1.0 {
		h = (4.0 - 4.0 * value) / 9.0
	} else {
		h = 0.0
	}
	return hsl_to_rgba(h_s_l(h, 1.0, 0.5))
}
