//
//  AppDelegate.swift
//  SuperVario
//
//  Created by Pedro Enrique on 2/27/17.
//  Copyright © 2017 Pedro Enrique. All rights reserved.
//

import UIKit
import CoreLocation
import AVFoundation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, DFBlunoDelegate, AVAudioSessionDelegate {
    
    var window: UIWindow?
    
    private let bleManager:DFBlunoManager! = DFBlunoManager.sharedInstance()
	
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        UIDevice.current.isBatteryMonitoringEnabled = true
        application.isIdleTimerDisabled = true
        self.bleManager.delegate = self
        self.bleManager.terminator = "|"
        self.bleManager.runOnMainThread = false
		let audioSession = AVAudioSession.sharedInstance()
        try! audioSession.setCategory(AVAudioSessionCategoryPlayback)
        try! audioSession.setActive(true)
		CommunicationManager.Instance.delegates.append(IGCFileRecorder.Instance)
	
		return true
    }
	

	
    func applicationWillResignActive(_ application: UIApplication) {
	}
    func applicationDidEnterBackground(_ application: UIApplication) {
        if !IGCFileRecorder.Instance.recording {
			exit(1)
		}
    }
	func applicationWillEnterForeground(_ application: UIApplication) { }
    func applicationDidBecomeActive(_ application: UIApplication) {
	}
    func applicationWillTerminate(_ application: UIApplication) { }
    func bleDidUpdateState(_ bleSupported: Bool) {
        if (bleSupported) {
            self.bleManager.scan()
        } else {
            print("Weird...")
        }
    }
    func didDiscover(_ dev: DFBlunoDevice!) {
        self.bleManager.stop()
        self.bleManager.connect(to: dev)
    }
    func didWriteData(_ dev: DFBlunoDevice!) { }
    func ready(toCommunicate dev: DFBlunoDevice!) { }
    
    func didDisconnectDevice(_ dev: DFBlunoDevice!)
    {
        arduino_tone(0)
        self.bleManager.scan()
    }

    func didReceive(_ data: Data?, device dev: DFBlunoDevice?)
    {
        if let incomingData = data {
            let incomingString = String(data: incomingData, encoding: String.Encoding.utf8)!
            CommunicationManager.Instance.update(message: incomingString)
        }
    }
}

